/*
BStone: A Source port of
Blake Stone: Aliens of Gold and Blake Stone: Planet Strike

Copyright (c) 1992-2013 Apogee Entertainment, LLC
Copyright (c) 2013-2015 Boris I. Bendovsky (bibendovsky@hotmail.com)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/


//
// Former SCALE.ASM
//


#include "3d_def.h"


extern word dc_iscale;
extern word dc_frac;
extern uint16_t dc_source;
extern uint8_t *dc_seg;
extern word dc_length;
extern int dc_dest;
/*extern int dc_x;
extern int dc_y;
extern int dc_dy;*/
extern word dc_width;

extern const uint8_t *shadingtable;

void R_DrawColumn(void)
{
	word frac = dc_frac;
	word iscale = dc_iscale;
	uint8_t *source = dc_seg + dc_source;
	uint8_t *destPtr = &vga_memory[dc_dest];
	int16_t count = dc_length;
	uint8_t pixel;

	switch (dc_width)
	{
		case 1:
#if 1
			do
			{
				*destPtr = source[frac >> 10];
				if (!(--count)) break;
				destPtr += vga_width;
				frac += iscale;
			} while (1);
#else
			do
			{
				*destPtr = source[frac >> 10];
				destPtr += vga_width;
				frac += iscale;
			} while (--count);
#endif
			break;

		case 2:
			do
			{
				pixel = source[frac >> 10];
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-1;
				frac += iscale;
			} while (--count);
			break;

		case 3:
			do
			{
				pixel = source[frac >> 10];
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-2;
				frac += iscale;
			} while (--count);
			break;

		case 4:
			do
			{
				pixel = source[frac >> 10];
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-3;
				frac += iscale;
			} while (--count);
			break;

		case 5:
			do
			{
				pixel = source[frac >> 10];
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-4;
				frac += iscale;
			} while (--count);
			break;

		case 6:
			do
			{
				pixel = source[frac >> 10];
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-5;
				frac += iscale;
			} while (--count);
			break;

		case 7:
			do
			{
				pixel = source[frac >> 10];
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-6;
				frac += iscale;
			} while (--count);
			break;

		case 8:
			do
			{
				pixel = source[frac >> 10];
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-7;
				frac += iscale;
			} while (--count);
			break;

		default:
			//Quit("%s dc_width %d\n", __FUNCTION__, dc_width);
			break;
	}
}

void R_DrawSLSColumn(void)
{
	uint8_t *destPtr = &vga_memory[dc_dest];
	int16_t count = dc_length;
	uint8_t pixel;
	const uint8_t *shadingPtr = shadingtable;

	switch (dc_width)
	{
		case 1:
			do
			{
				*destPtr = shadingPtr[0x1000 | *destPtr];
				destPtr += vga_width;
			} while (--count);
			break;

		case 2:
			do
			{
				pixel = shadingPtr[0x1000 | *destPtr];
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-1;
			} while (--count);
			break;

		case 3:
			do
			{
				pixel = shadingPtr[0x1000 | *destPtr];
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-2;
			} while (--count);
			break;

		case 4:
			do
			{
				pixel = shadingPtr[0x1000 | *destPtr];
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-3;
			} while (--count);
			break;

		case 5:
			do
			{
				pixel = shadingPtr[0x1000 | *destPtr];
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-4;
			} while (--count);
			break;

		case 6:
			do
			{
				pixel = shadingPtr[0x1000 | *destPtr];
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-5;
			} while (--count);
			break;

		case 7:
			do
			{
				pixel = shadingPtr[0x1000 | *destPtr];
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-6;
			} while (--count);
			break;

		case 8:
			do
			{
				pixel = shadingPtr[0x1000 | *destPtr];
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-7;
			} while (--count);
			break;
	}
}

void R_DrawLSColumn(void)
{
	word frac = dc_frac;
	word iscale = dc_iscale;
	uint8_t *source = dc_seg + dc_source;
	uint8_t *destPtr = &vga_memory[dc_dest];
	int16_t count = dc_length;
	uint8_t pixel;
	const uint8_t *shadingPtr = shadingtable;

	switch (dc_width)
	{
		case 1:
			do
			{
				*destPtr = shadingPtr[source[frac >> 10]];
				destPtr += vga_width;
				frac += iscale;
			} while (--count);
			break;

		case 2:
			do
			{
				pixel = shadingPtr[source[frac >> 10]];
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-1;
				frac += iscale;
			} while (--count);
			break;

		case 3:
			do
			{
				pixel = shadingPtr[source[frac >> 10]];
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-2;
				frac += iscale;
			} while (--count);
			break;

		case 4:
			do
			{
				pixel = shadingPtr[source[frac >> 10]];
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-3;
				frac += iscale;
			} while (--count);
			break;

		case 5:
			do
			{
				pixel = shadingPtr[source[frac >> 10]];
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-4;
				frac += iscale;
			} while (--count);
			break;

		case 6:
			do
			{
				pixel = shadingPtr[source[frac >> 10]];
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-5;
				frac += iscale;
			} while (--count);
			break;

		case 7:
			do
			{
				pixel = shadingPtr[source[frac >> 10]];
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-6;
				frac += iscale;
			} while (--count);
			break;

		case 8:
			do
			{
				pixel = shadingPtr[source[frac >> 10]];
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr++ = pixel;
				*destPtr = pixel;
				destPtr += vga_width-7;
				frac += iscale;
			} while (--count);
			break;
	}
}
