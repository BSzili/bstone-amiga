/*
BStone: A Source port of
Blake Stone: Aliens of Gold and Blake Stone: Planet Strike

Copyright (c) 1992-2013 Apogee Entertainment, LLC
Copyright (c) 2013-2015 Boris I. Bendovsky (bibendovsky@hotmail.com)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#include "3d_def.h"


typedef enum ShapeDrawMode {
	e_sdm_simple,
	e_sdm_shaded
} ShapeDrawMode;


extern const uint8_t *shadingtable;
extern const uint8_t *lightsource;

void R_DrawSLSColumn(void);
void R_DrawLSColumn(void);
void R_DrawColumn(void);


#ifdef GAMEVER_RESTORATION_AOG
#define CLOAKED_SHAPES (0)
#else
#define CLOAKED_SHAPES (1)
#endif

/*
=============================================================================

 GLOBALS

=============================================================================
*/

#ifdef COMPILED_SCALERS
t_compscale _seg *scaledirectory[MAXSCALEHEIGHT+1];
#endif
int16_t maxscale;
int16_t maxscaleshl2;
uint16_t centery;
word scalediv[MAXSCALEHEIGHT*2]; 

int16_t normalshade;
#ifdef GAMEVER_RESTORATION_AOG
int16_t normalshade_div = SHADE_DIV;
int16_t shade_max = SHADE_MAX;
#else
int16_t normalshade_div = 1;
int16_t shade_max = 1;
#endif

#ifndef GAMEVER_RESTORATION_AOG
int16_t nsd_table[] = { 1, 6, 3, 4, 1, 2 };
int16_t sm_table[] = { 36, 51, 62, 63, 18, 52 };
#endif
uint16_t *linecmds;


/*
=============================================================================

						  LOCALS

=============================================================================
*/

#ifdef COMPILED_SCALERS
t_compscale 	_seg *work;
uint16_t BuildCompScale (int16_t height, memptr *finalspot);

int16_t			stepbytwo;
#endif

//===========================================================================

/*
==============
=
= BadScale
=
==============
*/

#ifdef COMPILED_SCALERS
void far BadScale (void)
{
	//SCALE_ERROR(BADSCALE_ERROR);
	// just don't draw anything here
}
#endif


/*
============================
=
FreeScaleDirectory
=
============================
*/

#ifdef COMPILED_SCALERS
void FreeScaleDirectory (void)
{
	int	i;

	for (i=1;i<MAXSCALEHEIGHT;i++)
	{
		if (scaledirectory[i])
		{
			MM_SetPurge ((memptr *)&scaledirectory[i],3);
			MM_SetLock ((memptr *)&scaledirectory[i],false);
		}
		if (i>=stepbytwo)
			i += 2;
	}
	_fmemset (scaledirectory,0,sizeof(scaledirectory));

	MM_SortMem ();
}
#endif


/*
==========================
=
= SetupScaling
=
==========================
*/

void SetupScaling(int16_t maxscaleheight)
{
	int16_t		i;

	maxscaleheight /= 2; // one scaler every two pixels

	maxscale = maxscaleheight - 1;
	maxscaleshl2 = maxscale * 4;
	normalshade = (3 * maxscale) / (4 * normalshade_div);
	centery = viewheight / 2;
	for (i = 1; i < MAXSCALEHEIGHT*2; i++)
	{
		//scalediv[i] = (64<<16) / (fixed)i;
		scalediv[i] = (word)((fixed)(64<<10) / (fixed)i);
		//printf("%s scalediv[%d] = %d\n", __FUNCTION__, i, scalediv[i]);
	}

#ifdef COMPILED_SCALERS
	static oldmaxscaleheight = 0;
	if (maxscaleheight == oldmaxscaleheight)
		return;
	oldmaxscaleheight = maxscaleheight;

//
// free up old scalers
//
	FreeScaleDirectory();

//
// build the compiled scalers
//
	stepbytwo = viewheight/2;	// save space by double stepping
	MM_GetPtr ((memptr *)&work,20000);

	uint32_t scalersize = 0;
	for (i=1;i<=maxscaleheight;i++)
	{
		scalersize += BuildCompScale (i*2,(memptr *)&scaledirectory[i]);
		if (i>=stepbytwo)
			i+= 2;
	}
	MM_FreePtr ((memptr *)&work);
	printf("%s compiled %d scalers with %d bytes\n", __FUNCTION__, maxscaleheight, scalersize);

//
// compact memory and lock down scalers
//
	MM_SortMem ();
	for (i=1;i<=maxscaleheight;i++)
	{
		MM_SetLock ((memptr *)&scaledirectory[i],true);
		/*
		fullscalefarcall[i] = (uint16_t)scaledirectory[i];
		fullscalefarcall[i] <<=16;
		fullscalefarcall[i] += scaledirectory[i]->codeofs[0];
		*/
		if (i>=stepbytwo)
		{
			scaledirectory[i+1] = scaledirectory[i];
			//fullscalefarcall[i+1] = fullscalefarcall[i];
			scaledirectory[i+2] = scaledirectory[i];
			//fullscalefarcall[i+2] = fullscalefarcall[i];
			i+=2;
		}
	}
	scaledirectory[0] = scaledirectory[1];
	//fullscalefarcall[0] = fullscalefarcall[1];

//
// check for oversize wall drawing
//
	for (i=maxscaleheight;i<MAXSCALEHEIGHT;i++)
		scaledirectory[i] = (t_compscale *)BadScale;
		//fullscalefarcall[i] = (int32_t)BadScale;
#endif
}

//===========================================================================

/*
========================
=
= BuildCompScale
=
= Builds a compiled scaler object that will scale a 64 tall object to
= the given height (centered vertically on the screen)
=
= height should be even
=
= Call with
= ---------
= A0		Source for scale
= A1		Dest for scale
=
= Calling the compiled scaler only destroys D0
=
========================
*/

#ifdef COMPILED_SCALERS
uint16_t BuildCompScale (int16_t height, memptr *finalspot)
{
	byte		far *code;

	int16_t			i;
	int32_t		fix,step;
	uint16_t	src,totalscaled,totalsize;
	int16_t			startpix,endpix,toppix;


	step = ((int32_t)height<<16) / 64;
#if 1
	code = work;
#else
	code = &work->code[0];
#endif
	toppix = (viewheight-height)/2;
	fix = 0;

	for (src=0;src<=64;src++)
	{
		startpix = fix>>16;
		fix += step;
		endpix = fix>>16;

#if 0
		if (endpix>startpix)
			work->width[src] = endpix-startpix;
		else
			work->width[src] = 0;
#endif

//
// mark the start of the code
//
#if 0
		work->codeofs[src] = FP_OFF(code);
#endif

//
// compile some code if the source pixel generates any screen pixels
//
		startpix+=toppix;
		endpix+=toppix;

		if (startpix == endpix || endpix < 0 || startpix >= viewheight || src == 64)
			continue;

#if 1
		if (endpix-startpix > 1)
		{
		//
		// move.b (src,a0),d0
		//
			*code++ = 0x10;
			*code++ = 0x28;
			*((int16_t far *)code) = src;
			code += 2;
		}
#else
	//
	// mov al,[si+src]
	//
		*code++ = 0x8a;
		*code++ = 0x44;
		*code++ = src;
#endif

		for (;startpix<endpix;startpix++)
		{
			if (startpix >= viewheight)
				break;						// off the bottom of the view area
			if (startpix < 0)
				continue;					// not into the view area

#if 1
			int16_t heightofs = (startpix-(vga_height/2))*vga_width;

			if (endpix-startpix > 1)
			{
			//
			// move.b d0,(heightofs,a1)
			//
				*code++ = 0x13;
				*code++ = 0x40;
				*((int16_t far *)code) = heightofs;
				code += 2;
			}
			else
			{
			//
			// move.b (src,a0),(heightofs,a1)
			//
				*code++ = 0x13;
				*code++ = 0x68;
				*((int16_t far *)code) = src;
				code += 2;
				*((int16_t far *)code) = heightofs;
				code += 2;
			}
#else
		//
		// mov [es:di+heightofs],al
		//
			*code++ = 0x26;
			*code++ = 0x88;
			*code++ = 0x85;
			*((uint16_t far *)code)++ = startpix*SCREENBWIDE;
#endif
		}

	}

#if 1
//
// rts
//
	*code++ = 0x4e;
	*code++ = 0x75;
#else
//
// retf
//
	*code++ = 0xcb;
#endif

#if 1
	totalsize = (code-(byte *)work);
#else
	totalsize = FP_OFF(code);
#endif
	MM_GetPtr (finalspot,totalsize);
	_fmemcpy ((byte _seg *)(*finalspot),(byte _seg *)work,totalsize);

	return totalsize;
}

#endif


// Draw Column vars

word dc_iscale;
word dc_frac;
uint16_t dc_source;
uint8_t *dc_seg;
word dc_length;
int dc_dest;
uint16_t dc_x;
//int dc_y; // TODO remove, refactor MegaSimpleScaleShape
//int dc_dy;
word dc_width;

static void (*DrawColumnFunc)(void);
static int16_t dc_sprtopoffset;
static word dc_screenstep;

//#define SFRACUNIT (0x10000)

#ifndef GAMEVER_RESTORATION_AOG
extern boolean useBounceOffset;

fixed bounceOffset = 0;
#endif

static void generic_scale_masked_post(void)
{
	const uint16_t *srcpost = linecmds;

	word screenstep = dc_screenstep;
	int16_t sprtopoffset = dc_sprtopoffset;

	uint16_t end = (*srcpost) / 2;
	srcpost++;

	int buf = vl_get_offset(bufferofs, dc_x, 0); // TODO uint16_t?

	while (end != 0)
	{
		dc_source = *srcpost;
		srcpost++;

		uint16_t start = *srcpost / 2;
		srcpost++;

		dc_source += start;

		int16_t length = end - start;
		int16_t topscreen = sprtopoffset + (screenstep * start);
		int16_t bottomscreen = topscreen + (screenstep * length);
		int16_t dc_yl = (topscreen + 0x80 - 1) >> 7;
		int16_t dc_yh = (bottomscreen - 1) >> 7;

		if (dc_yh >= viewheight)
			dc_yh = viewheight - 1;

		if (dc_yl < 0)
		{
			dc_frac = dc_iscale * (word)(-dc_yl);
			dc_yl = 0;
		}
		else
		{
			dc_frac = 0;
		}

		if (dc_yl <= dc_yh)
		{
			//dc_dest = buf + ylookup[dc_yl];
			dc_dest = buf + vga_width*dc_yl;
			dc_length = dc_yh - dc_yl + 1;
			DrawColumnFunc();
		}

		end = *srcpost / 2;
		srcpost++;
	}
}

static void generic_scale_shape(int16_t xcenter, int16_t shapenum, uint16_t height, int8_t lighting, ShapeDrawMode draw_mode)
{
	if ((height / 2) > maxscaleshl2 || ((height / 2) == 0))
		return;

	t_compshape *shape = (t_compshape *)PM_GetSpritePage(shapenum);

	int32_t xscale = (int32_t)height << 12;

	int32_t xcent = ((int32_t)xcenter << 20) - ((int32_t)height << 17) + 0x80000;

	//
	// calculate edges of the shape
	//
	//int x1 = (int)((xcent + (shape->leftpix * xscale)) >> 20);
	int16_t x1 = (int16_t)((int32_t)(xcent+((int32_t)shape->leftpix*xscale))>>20);

	if (x1 >= viewwidth)
		return; // off the right side

	//int x2 = (int)((xcent + (shape->rightpix * xscale)) >> 20);
	int16_t x2 = (int16_t)((int32_t)(xcent+((int32_t)shape->rightpix*xscale))>>20);

	if (x2 < 0)
		return; // off the left side

	int quarterheight = height/4;
	//longword screenscale = (256 << 20) / height;
	word screenscale = scalediv[quarterheight];

	//
	// store information in a vissprite
	//
	word frac;

	if (x1 < 0)
	{
		frac = (-x1) * screenscale;
		x1 = 0;
	}
	else
	{
		frac = screenscale / 2;
	}

	if (x2 >= viewwidth)
		x2 = viewwidth - 1;
//cloaked_shape = 1; // hack
	if (draw_mode == e_sdm_shaded)
	{
#ifdef GAMEVER_RESTORATION_AOG
		int16_t i=shade_max-(63l*(int32_t)(height>>3)/(int32_t)normalshade)+lighting;
#else
		int16_t i=shade_max-(63l*(uint32_t)(height>>3)/(uint32_t)normalshade)+lighting;
#endif

		if (i < 0)
			i = 0;
		else
#ifdef GAMEVER_RESTORATION_AOG
			i %= 64;
#else
		if (i > 63)
			i = 63;
#endif

#if CLOAKED_SHAPES
		if (cloaked_shape)
		{
			shadingtable = &lightsource[i * 256];
			DrawColumnFunc = R_DrawSLSColumn;
		}
		else
#endif
		if (i > 0)
		{
			shadingtable = &lightsource[i * 256];
			DrawColumnFunc = R_DrawLSColumn;
		}
		else
		{
			DrawColumnFunc = R_DrawColumn;
		}
	}
	else
	{
		DrawColumnFunc = R_DrawColumn;
	}

	uint16_t swidth = shape->rightpix - shape->leftpix;
	dc_seg = (uint8_t *)(shape);
	uint16_t *dataofs = shape->dataofs;
	//dc_iscale = (64 * 65536) / quarterheight;
	//dc_iscale = scalediv[quarterheight];
	dc_iscale = screenscale;
	dc_sprtopoffset = (viewheight << 6) - (quarterheight << 6);
	dc_screenstep = quarterheight << 1;

	if (height>256)
	{
		int16_t lastcolumn = -1;

		dc_width = 1;
		//dc_x = 0;

		for (; x1 <= x2; ++x1, frac += screenscale)
		{
			if (wallheight[x1] > height)
			{
				if (lastcolumn != -1)
				{
					linecmds = (uint16_t *)(&dc_seg[dataofs[lastcolumn]]);
					generic_scale_masked_post();
					//dc_width = 1;
					lastcolumn = -1;
				}
				continue;
			}

			int16_t texturecolumn = frac >> 10;

			if (texturecolumn > swidth)
				texturecolumn = swidth;

			if (texturecolumn == lastcolumn)
			{
				dc_width++;
				continue;
			}
			else
			{
				if (lastcolumn != -1)
				{
					linecmds = (uint16_t *)(&dc_seg[dataofs[lastcolumn]]);
					generic_scale_masked_post();
				}
				dc_x = x1;
				dc_width = 1;
				lastcolumn = texturecolumn;
			}
		}
		if (lastcolumn != -1)
		{
			linecmds = (uint16_t *)(&dc_seg[dataofs[lastcolumn]]);
			generic_scale_masked_post();
		}
	}
	else
	{
		dc_width = 1;

		for (; x1 <= x2; ++x1, frac += screenscale)
		{
			if (wallheight[x1] > height)
				continue;

			dc_x = x1;

			int16_t texturecolumn = frac >> 10;

			if (texturecolumn > swidth)
				texturecolumn = swidth;

			linecmds = (uint16_t *)(&dc_seg[dataofs[texturecolumn]]);

			generic_scale_masked_post();
		}
	}
//cloaked_shape = 0; // hack
}

/*
=======================
=
= ScaleLSShape with Light sourcing
=
= Draws a compiled shape at [scale] pixels high
=
= each vertical line of the shape has a pointer to segment data:
=       end of segment pixel*2 (0 terminates line) used to patch rtl in scaler
=       top of virtual line with segment in proper place
=       start of segment pixel*2, used to jsl into compiled scaler
=       <repeat>
=
= Setup for call
= --------------
= GC_MODE read mode 1, write mode 2
= GC_COLORDONTCARE  set to 0, so all reads from video memory return 0xff
= GC_INDEX pointing at GC_BITMASK
=
=======================
*/
void ScaleLSShape(int16_t xcenter, int16_t shapenum, uint16_t height, int8_t lighting)
{
	generic_scale_shape(xcenter, shapenum, height, lighting, e_sdm_shaded);
}

/*
=======================
=
= ScaleShape
=
= Draws a compiled shape at [scale] pixels high
=
= each vertical line of the shape has a pointer to segment data:
=       end of segment pixel*2 (0 terminates line) used to patch rtl in scaler
=       top of virtual line with segment in proper place
=       start of segment pixel*2, used to jsl into compiled scaler
=       <repeat>
=
= Setup for call
= --------------
= GC_MODE read mode 1, write mode 2
= GC_COLORDONTCARE  set to 0, so all reads from video memory return 0xff
= GC_INDEX pointing at GC_BITMASK
=
=======================
*/
void ScaleShape(int16_t xcenter, int16_t shapenum, uint16_t height)
{
	generic_scale_shape(xcenter, shapenum, height, 0, e_sdm_simple);
}

/*
=======================
=
= SimpleScaleShape
=
= NO CLIPPING, height in pixels
=
= Draws a compiled shape at [scale] pixels high
=
= each vertical line of the shape has a pointer to segment data:
=       end of segment pixel*2 (0 terminates line) used to patch rtl in scaler
=       top of virtual line with segment in proper place
=       start of segment pixel*2, used to jsl into compiled scaler
=       <repeat>
=
= Setup for call
= --------------
= GC_MODE read mode 1, write mode 2
= GC_COLORDONTCARE  set to 0, so all reads from video memory return 0xff
= GC_INDEX pointing at GC_BITMASK
=
=======================
*/
void SimpleScaleShape(int16_t xcenter, int16_t shapenum, uint16_t height)
{
	t_compshape *shape = (t_compshape *)PM_GetSpritePage(shapenum);

	int16_t xscale = (int16_t)height;
	int16_t xcent = ((int16_t)xcenter << 6) - ((int16_t)height << 5) + 0x40;

	//
	// calculate edges of the shape
	//
	//int x1 = (int)((xcent + (shape->leftpix * xscale)) >> 16);
	int16_t x1 = (int16_t)(xcent+((int16_t)shape->leftpix*xscale))>>6;

	if (x1 >= viewwidth)
		return; // off the right side

	//int x2 = (int)((xcent + (shape->rightpix * xscale)) >> 16);
	int16_t x2 = (int16_t)(xcent+((int16_t)(shape->rightpix+1)*xscale))>>6;

	if (x2 < 0)
		return; // off the left side

	word screenscale = scalediv[height];

	//
	// store information in a vissprite
	//
	word frac;

	if (x1 < 0)
	{
		frac = screenscale * (-x1);
		x1 = 0;
	}
	else
	{
		frac = screenscale / 2;
	}

	if (x2 >= viewwidth)
		x2 = viewwidth - 1;

	DrawColumnFunc = R_DrawColumn;
	dc_iscale = screenscale;
	dc_seg = (uint8_t *)(shape);
	uint16_t *dataofs = shape->dataofs;
	dc_sprtopoffset = (viewheight << 6) - (height << 6);
	dc_screenstep = height << 1;
	uint16_t swidth = shape->rightpix - shape->leftpix;

	if (height>64)
	{
		int16_t lastcolumn = -1;

		//dc_x = 0;
		dc_width = 1;

		for (; x1 < x2; ++x1, frac += screenscale)
		{
			int16_t texturecolumn = frac >> 10;

			if (texturecolumn > swidth)
				texturecolumn = swidth;

			if (texturecolumn == lastcolumn)
			{
				dc_width++;
				continue;
			}
			else
			{
				if (lastcolumn != -1)
				{
					linecmds = (uint16_t *)(&dc_seg[dataofs[lastcolumn]]);
					generic_scale_masked_post();
				}
				dc_width = 1;
				dc_x = x1;
				lastcolumn = texturecolumn;
			}
		}
		if (lastcolumn != -1)
		{
			linecmds = (uint16_t *)(&dc_seg[dataofs[lastcolumn]]);
			generic_scale_masked_post();
		}
	}
	else
	{
		dc_width = 1;

		for (; x1 < x2; ++x1, frac += screenscale)
		{
			dc_x = x1;

			int16_t texturecolumn = frac >> 10;

			if (texturecolumn > swidth)
				texturecolumn = swidth;

			linecmds = (uint16_t *)(&dc_seg[dataofs[texturecolumn]]);

			generic_scale_masked_post();
		}
	}
}

// -------------------------------------------------------------------------
// MegaSimpleScaleShape()
//
// NOTE: Parameter SHADE determines which Shade palette to use on the shape.
//       0 == NO Shading
//       63 == Max Shade (BLACK or near)
// -------------------------------------------------------------------------
#ifdef GAMEVER_RESTORATION_AOG
void MegaSimpleScaleShape (int16_t xcenter, int16_t ycenter, int16_t shapenum, uint16_t height)
#else
void MegaSimpleScaleShape (int16_t xcenter, int16_t ycenter, int16_t shapenum, uint16_t height, uint16_t shade)
#endif
{
	uint16_t old_bufferofs = bufferofs;
	ycenter -= 34;
	bufferofs -= ((viewheight-64)>>1)*SCREENBWIDE;
	bufferofs += SCREENBWIDE*ycenter;

	t_compshape *shape = (t_compshape *)PM_GetSpritePage(shapenum);

	/*
	int32_t xscale = (int32_t)(height) << 14;
	int32_t xcent = ((int32_t)(xcenter) << 20) - ((int32_t)(height) << 19) + 0x80000;
	*/
	//int16_t xscale = (int16_t)height >> 2;
	int16_t xscale = (int16_t)height;
	int16_t xcent = ((int16_t)xcenter << 6) - ((int16_t)height << 5) + 0x40;

	//
	// calculate edges of the shape
	//
	//int x1 = (int)((xcent + (shape->leftpix * xscale)) >> 20);
	int16_t x1 = (int16_t)(xcent+((int16_t)shape->leftpix*xscale))>>6;

	if (x1 >= viewwidth)
		return; // off the right side

	//int x2 = (int)((xcent + (shape->rightpix * xscale)) >> 20);
	int16_t x2 = (int16_t)(xcent+((int16_t)(shape->rightpix+1)*xscale))>>6;

	if (x2 < 0)
		return; // off the left side

	//longword screenscale = (64 << 20) / height;
	//word screenscale = scalediv[height/4];
	word screenscale = scalediv[height];

#ifndef GAMEVER_RESTORATION_AOG
	//
	// Choose shade table.
	//
	if (shade)
	{
		shadingtable = &lightsource[shade * 256];
		DrawColumnFunc = R_DrawLSColumn;
	}
	else
#endif
	{
		DrawColumnFunc = R_DrawColumn;
	}

	//
	// store information in a vissprite
	//
	word frac;

	if (x1 < 0)
	{
		frac = screenscale * (-x1);
		x1 = 0;
	}
	else
	{
		frac = screenscale / 2;
	}

	if (x2 >= viewwidth)
		x2 = viewwidth - 1;

	//dc_iscale = (64 * 65536) / height;
	dc_iscale = scalediv[height];
	dc_seg = (uint8_t *)(shape);
	uint16_t *dataofs = shape->dataofs;
#ifndef GAMEVER_RESTORATION_AOG
	if (useBounceOffset)
		dc_sprtopoffset = (viewheight << 6) - (height << 6) + (bounceOffset >> 10);
	else
#endif
		dc_sprtopoffset = (viewheight << 6) - (height << 6);
	dc_screenstep = height << 1;
	uint16_t swidth = shape->rightpix - shape->leftpix;

	if (height>64)
	{
		int16_t lastcolumn = -1;

		//dc_x = 0;
		dc_width = 1;

		for (; x1 < x2; ++x1, frac += screenscale)
		{
			int16_t texturecolumn = frac >> 10;

			if (texturecolumn > swidth)
				texturecolumn = swidth;

			if (texturecolumn == lastcolumn)
			{
				dc_width++;
				continue;
			}
			else
			{
				if (lastcolumn != -1)
				{
					linecmds = (uint16_t *)(&dc_seg[dataofs[lastcolumn]]);
					generic_scale_masked_post();
				}
				dc_width = 1;
				dc_x = x1;
				lastcolumn = texturecolumn;
			}
		}
		if (lastcolumn != -1)
		{
			linecmds = (uint16_t *)(&dc_seg[dataofs[lastcolumn]]);
			generic_scale_masked_post();
		}
	}
	else
	{
		dc_width = 1;

		for (; x1 < x2; ++x1, frac += screenscale)
		{
			dc_x = x1;

			int16_t texturecolumn = frac >> 10;

			if (texturecolumn > swidth)
				texturecolumn = swidth;

			linecmds = (uint16_t *)(&dc_seg[dataofs[texturecolumn]]);
			generic_scale_masked_post();
		}
	}
	bufferofs = old_bufferofs;
}
