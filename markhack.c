/*
BStone: A Source port of
Blake Stone: Aliens of Gold and Blake Stone: Planet Strike

Copyright (c) 1992-2013 Apogee Entertainment, LLC
Copyright (c) 2013-2015 Boris I. Bendovsky (bibendovsky@hotmail.com)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/


//
// Former MARKHACK.ASM
//


#include "3d_def.h"


extern uint16_t postheight;

extern word dc_iscale;
extern word dc_frac;
extern uint16_t dc_source;
extern uint8_t *dc_seg;
extern word dc_length;
extern int dc_dest;
extern word dc_width;
extern word scalediv[];

//
// Draws an unmasked post centered in the viewport
//

void DrawPost()
{
#ifdef COMPILED_SCALERS
	// try the compiler scalers
	if (postwidth == 1 && postheight < (uint16_t)maxscale)
	{
		t_compscale *compscaler = scaledirectory[postheight];
		uint8_t *src = postsource;
		uint8_t *dest = &vga_memory[vl_get_offset(bufferofs, postx, vga_height/2)];
		void (*scalefunc)(uint8_t *src __asm("a0"), uint8_t *dest __asm("a1")) = (void *)compscaler;
		scalefunc(src, dest);
		return;
	}
#endif
	dc_length = postheight*2;
	//dc_iscale = (64<<16) / (fixed)dc_length;
	dc_iscale = scalediv[dc_length];

	if (dc_length > viewheight)
	{
		dc_frac = (dc_length-viewheight)/2*dc_iscale;
		dc_length = viewheight;
		dc_dest = vl_get_offset(bufferofs, postx, 0);
	}
	else
	{
		int toppix = (viewheight-dc_length)/2;
		dc_frac = 0;
		dc_dest = vl_get_offset(bufferofs, postx, toppix);
	}

	//dc_dest = vl_get_offset(bufferofs, postx, toppix);
	dc_seg = postsource;
	dc_source = 0;
	dc_width = postwidth;

	R_DrawColumn();
}


//
// Draws an unmasked light sourced post centered in the viewport
//

void DrawLSPost()
{
	dc_length = postheight*2;
	//dc_iscale = (64<<16) / (fixed)dc_length;
	dc_iscale = scalediv[dc_length];

	if (dc_length > viewheight)
	{
		dc_frac = (dc_length-viewheight)/2*dc_iscale;
		dc_length = viewheight;
		//toppix = 0;
		dc_dest = vl_get_offset(bufferofs, postx, 0);
	}
	else
	{
		int toppix = (viewheight-dc_length)/2;
		dc_frac = 0;
		dc_dest = vl_get_offset(bufferofs, postx, toppix);
	}

	//dc_dest = vl_get_offset(bufferofs, postx, toppix);
	dc_seg = postsource;
	dc_source = 0;
	dc_width = postwidth;

	R_DrawLSColumn();
}
