//
//	ID Engine
//	ID_SD.c - Sound Manager for Wolfenstein 3D
//	v1.3 (revised for **********, screwed with for Blake Stone)
//	By Jason Blochowiak
//

//
//	This module handles dealing with generating sound on the appropriate
//		hardware
//
//	Depends on: User Mgr (for parm checking)
//
//	Globals:
//		For User Mgr:
//			SoundSourcePresent - Sound Source thingie present?
//			SoundBlasterPresent - SoundBlaster card present?
//			AdLibPresent - AdLib card present?
//			SoundMode - What device is used for sound effects
//				(Use SM_SetSoundMode() to set)
//			MusicMode - What device is used for music
//				(Use SM_SetMusicMode() to set)
//			DigiMode - What device is used for digitized sound effects
//				(Use SM_SetDigiDevice() to set)
//
//		For Cache Mgr:
//			NeedsDigitized - load digitized sounds?
//			NeedsMusic - load music?
//

#include <proto/exec.h>
#include <proto/lowlevel.h>
#include <proto/ahi.h>
#include <devices/ahi.h>
#include <SDI_hook.h>

#define Point BSPoint
#include "id_heads.h"

//	Internal variables

int16_t		DigiMap[LASTSOUND];

//	Global variables
	boolean		SoundSourcePresent,
				AdLibPresent,
				SoundBlasterPresent,SBProPresent,
				NeedsDigitized,NeedsMusic,
				SoundPositioned;
	SDMode		SoundMode;
	SMMode		MusicMode;
	SDSMode		DigiMode;
	longword	TimeCount;
	SoundCommon		**SoundTable;

//	Internal variables
static	boolean			SD_Started;
		boolean			nextsoundpos;
static	void			(*SoundUserHook)(void);
		soundnames		SoundNumber,DigiNumber;
		word			SoundPriority,DigiPriority;
		int16_t				LeftPosition,RightPosition;

		word				NumDigi;
		word				_seg *DigiList;
		volatile boolean	DigiPlaying;

//	Sequencer variables
		boolean			sqActive;
		boolean			sqPlayedOnce;

//	Internal routines

// Sound
struct Library      *AHIBase;
static struct MsgPort      *AHImp     = NULL;
static struct AHIRequest   *AHIio     = NULL;
static BYTE                 AHIDevice = -1;
static struct AHIAudioCtrl *actrl     = NULL;
static uint32_t digivol = 0x10000;
static uint32_t digipan = 0x8000;

typedef struct
{
	int8_t *data;
	int32_t length;
	int32_t rate;
} sample_t;

enum
{
	CHANNEL_DIGI,
	CHANNEL_ADLIB,
	CHANNEL_MUSIC,
	CHANNEL_MAX
};
enum
{
	SOUND_DIGI,
	SOUND_ADLIB,
	SOUND_MUSIC,
	SOUND_MAX
}; // TODO this is a bit nasty

static struct
{
	struct AHIEffChannelInfo aeci;
	ULONG offsets[CHANNEL_MAX];
} aci;

// SB frequncy divisor 114: (1000000) / (256 - divisor)
#define DIGI_SAMPLE_RATE 7042

static ULONG channelFlags[CHANNEL_MAX] = {0};
#define FLAG_PLAYEDONCE (1<<31)
#define FLAG_ISPLAYING (1<<30)
#define CHANNELFLAGMASK (FLAG_PLAYEDONCE|FLAG_ISPLAYING)

HOOKPROTO(BEL_ST_EffectFunc, void, struct AHIAudioCtrl *ac, struct AHIEffChannelInfo *ci)
{
	UWORD i;

	for (i = 0; i < ci->ahieci_Channels; i++)
	{
		ULONG offset = ci->ahieci_Offset[i];
		ULONG lastOffset = channelFlags[i] & (~CHANNELFLAGMASK);
		ULONG flags = channelFlags[i] & CHANNELFLAGMASK;
		channelFlags[i] = flags | offset;
		//printf("i %d flags %d offset %d lastOffset %d\n", i, flags, offset, lastOffset);
		if (offset < lastOffset) // buffer wraparound
			channelFlags[i] |= FLAG_PLAYEDONCE;
		//else if (offset == 0 && lastOffset == 0)
		else if (offset == lastOffset)
			channelFlags[i] &= ~FLAG_ISPLAYING;
	}
}
MakeStaticHook(g_effectHook, BEL_ST_EffectFunc);

int BE_ST_InitAudio(void)
{
	if ((AHImp = CreateMsgPort()))
	{
		if ((AHIio = (struct AHIRequest *)CreateIORequest(AHImp, sizeof(struct AHIRequest))))
		{
			AHIio->ahir_Version = 4;
			if (!(AHIDevice = OpenDevice((STRPTR)AHINAME, AHI_NO_UNIT, (struct IORequest *)AHIio, 0)))
			{
				//ULONG id = AHI_INVALID_ID;

				AHIBase = (struct Library *)AHIio->ahir_Std.io_Device;

				/*
				id = AHI_BestAudioID(
					AHIDB_Volume, TRUE,
					AHIDB_Panning, TRUE,
					AHIDB_Bits, 8,
					AHIDB_MinMixFreq, 7000,
					TAG_END);
					*/

				actrl = AHI_AllocAudio(
					//AHIA_AudioID, id,
					AHIA_Channels, CHANNEL_MAX,
					AHIA_Sounds, SOUND_MAX,
					TAG_END);

				if (actrl)
				{
					ULONG error = AHI_ControlAudio(actrl,
						AHIC_Play, TRUE,
						TAG_END);

					char modename[64];
					ULONG volume, panning;
					AHI_GetAudioAttrs(AHI_INVALID_ID, actrl,
								AHIDB_BufferLen, sizeof(modename),
								AHIDB_Name, (Tag)modename,
								AHIDB_Volume, (Tag)&volume,
								AHIDB_Panning, (Tag)&panning,
								/*
								AHIDB_MaxChannels, (Tag)&channels,
								AHIDB_Bits, (Tag)&bits,
								*/
								TAG_END);

					SBProPresent = volume && panning;

					ULONG mixfreq;
					AHI_ControlAudio(actrl,
						AHIC_MixFreq_Query, (Tag)&mixfreq,
						TAG_END);

					aci.aeci.ahie_Effect = AHIET_CHANNELINFO;
					aci.aeci.ahieci_Func = &g_effectHook;
					aci.aeci.ahieci_Channels = CHANNEL_MAX;
					AHI_SetEffect(&aci.aeci, actrl);

					printf("AHI mode '%s' mix.freq. %lu volume %lu panning %lu\n", modename, mixfreq, volume, panning);

					return error == AHIE_OK;
				}
			}
		}
	}

	printf("%s Couldn't initialize AHI\n", __FUNCTION__);

	return 0;
}

void BE_ST_ShutdownAudio(void)
{
	if (AHIBase && actrl)
	{
		aci.aeci.ahie_Effect = AHIET_CHANNELINFO|AHIET_CANCEL;
		AHI_SetEffect(&aci.aeci, actrl);

		AHI_ControlAudio(actrl,
			AHIC_Play, FALSE,
			TAG_END);

		AHI_FreeAudio(actrl);
		actrl = NULL;
	}

	if (!AHIDevice)
	{
		AHIBase = NULL;
		CloseDevice((struct IORequest *)AHIio);
		AHIDevice = -1;
	}

	if (AHIio)
	{
		DeleteIORequest((struct IORequest *)AHIio);
		AHIio = NULL;
	}

	if (AHImp)
	{
		DeleteMsgPort(AHImp);
		AHImp = NULL;
	}
}

static inline boolean BEL_ST_LoadSample(int8_t *data, int32_t length, int16_t sound)
{
	struct AHISampleInfo sample;
	ULONG error;

	//printf("%s(%p,%d)\n", __FUNCTION__, data, length);

	if (!actrl || !data)
		return false;

	sample.ahisi_Address = data;
	sample.ahisi_Type = AHIST_M8S;
	sample.ahisi_Length = length;
	AHI_UnloadSound(sound, actrl);
	error = AHI_LoadSound(sound, AHIST_SAMPLE, &sample, actrl);

	return error == AHIE_OK;
}

static inline void BEL_ST_PlaySample(int16_t channel, int16_t sound, int32_t length, int32_t rate, uint32_t vol, uint32_t pan, boolean loop)
{
	//printf("%s(%d,%d)\n", __FUNCTION__, channel, length);

	if (!actrl)
		return;

	channelFlags[channel] = FLAG_ISPLAYING;
	AHI_SetFreq(channel, rate, actrl, AHISF_IMM);
	AHI_SetVol(channel, vol, pan, actrl, AHISF_IMM);
	AHI_SetSound(channel, sound, 0, length, actrl, AHISF_IMM);
	if (!loop)
		AHI_SetSound(channel, AHI_NOSOUND, 0, 0, actrl, AHISF_NONE);
}

static inline void BEL_ST_StopSample(int16_t channel)
{
	if (!actrl)
		return;

	channelFlags[channel] = 0;
	AHI_SetSound(channel, AHI_NOSOUND, 0, 0, actrl, AHISF_IMM);
}

// Digi sounds
#define MAXDIGINUM 58
static sample_t g_digiSounds[MAXDIGINUM];

// AdLib sounds
#define LASTSOUND 100 // for both games
static sample_t g_adlibSounds[LASTSOUND+1]; // LASTSOUND is reserved for the music

typedef struct
{
	uint32_t magic; /* magic number */
	uint32_t hdr_size; /* size of this header */ 
	uint32_t data_size; /* length of data (optional) */ 
	uint32_t encoding; /* data encoding format */
	uint32_t sample_rate; /* samples per second */
	uint32_t channels; /* number of interleaved channels */
} Audio_filehdr;

static void BEL_ST_LoadSoundFile(char *filename, sample_t *digi)
{
	int fp;

	if ((fp = open(filename, O_RDONLY, S_IREAD)) != -1)
	{
		Audio_filehdr header;
		read(fp, &header, sizeof(header));
		int32_t nbyte = header.data_size;
		if (nbyte == -1)
		{
			nbyte = filelength(fp);
			nbyte -= header.hdr_size;
		}
		MM_BombOnError (false);
		MM_GetPtr((memptr)&digi->data, nbyte);
		MM_BombOnError (true);
		if (mmerror)
			mmerror = false;
		else
		{
			read(fp, digi->data, nbyte);
			digi->length = nbyte;
			digi->rate = header.sample_rate;
			MM_SetPurge((memptr)&digi->data, 3);
		}
		close(fp);
	}
}

static void BEL_ST_LoadSound(int sound)
{
	char filename[32];
	snprintf(filename, sizeof(filename), "adlib/sound%02d.au", sound);
	//printf("%s loading %s...\n", __FUNCTION__, filename);
	BEL_ST_LoadSoundFile(filename, &g_adlibSounds[sound]);
}

static void BEL_ST_FreeSound(sample_t *digi)
{
	if (!digi->data)
		return;

	MM_FreePtr((memptr)&digi->data);
}

// AdLib music
static void BEL_ST_FreeMusic(void)
{
	BEL_ST_FreeSound(&g_adlibSounds[LASTSOUND]);
}

static void BEL_ST_LoadMusic(int music)
{
	char filename[32];
	snprintf(filename, sizeof(filename), "adlib/music%02d.au", music);
	BEL_ST_LoadSoundFile(filename, &g_adlibSounds[LASTSOUND]);
	if (!g_adlibSounds[LASTSOUND].data)
	{
		return;
	}
	MM_SetLock((memptr)&g_adlibSounds[LASTSOUND].data, true); // don't let the music to be purged!
	BEL_ST_LoadSample(g_adlibSounds[LASTSOUND].data, g_adlibSounds[LASTSOUND].length, SOUND_MUSIC);
}

// Timer
static APTR g_timerIntHandle = NULL;

//
//	Stuff for digitized sounds
//

static void
SDL_DigitizedDone(void)
{
	if (DigiNumber)
	{
		sample_t *digi = &g_digiSounds[DigiNumber];
		MM_SetLock((memptr)&digi->data, false);
	}
	DigiNumber = DigiPriority = 0;
	DigiPlaying = SoundPositioned = false;
}

void
SD_StopDigitized(void)
{
	BEL_ST_StopSample(CHANNEL_DIGI);

	SDL_DigitizedDone();
}

void
SD_Poll(void)
{
	if ((DigiPriority || DigiNumber) && !(channelFlags[CHANNEL_DIGI] & FLAG_ISPLAYING))
	{
		SDL_DigitizedDone();
	}
}

void
SD_SetPosition(int16_t leftpos,int16_t rightpos)
{
	if (DigiMode == sds_Off)
		return;

	if (!SBProPresent)
		return;

	// convert 0-8 positions to volumes 0-15, sum them and transform to 0-65536
	digivol = ((15-leftpos)+(15-rightpos))*65536/30;
	// convert the channel volumes into panning
	digipan = (16-leftpos+rightpos)*2048;
	//digipan = (8-leftpos+rightpos)*4096; // maximum stereo separation

	if (!actrl)
		return;

	if (channelFlags[CHANNEL_DIGI] & FLAG_ISPLAYING)
		AHI_SetVol(CHANNEL_DIGI, digivol, digipan, actrl, AHISF_IMM);
}

static void
SDL_LoadDigiSound(word which)
{
	sample_t *digi = &g_digiSounds[which];

	if (which < 0 || which >= MAXDIGINUM)
		return;

	word DigiPage = DigiList[(which * 2) + 0];
	word DigiLeft = DigiList[(which * 2) + 1];
	//printf("Digi %d page %d size %d\n", which, DigiPage, DigiLeft);

	MM_GetPtr((memptr)&digi->data, DigiLeft);
	digi->length = DigiLeft;
	digi->rate = DIGI_SAMPLE_RATE;

	char *dst = (char *)digi->data;
	do
	{
		word DigiNextLen = (DigiLeft >= PMPageSize)? PMPageSize : (DigiLeft % PMPageSize);
		DigiLeft -= DigiNextLen;
		memptr DigiNextAddr = PM_GetSoundPage(DigiPage);
		PM_SetPageLock(PMSoundStart + DigiPage,pml_Locked);
		//printf("copying %d bytes...\n", DigiNextLen);
		memcpy(dst, DigiNextAddr, DigiNextLen);
		PM_SetPageLock(PMSoundStart + DigiPage,pml_Unlocked);
		DigiPage++;
		dst += DigiNextLen;
	} while (DigiLeft);

	MM_SetPurge((memptr)&digi->data, 3);
}

static void
SDL_FreeDigiSound(int sound)
{
	sample_t *digi = &g_digiSounds[sound];

	if (sound < 0 || sound >= MAXDIGINUM)
		return;

	MM_FreePtr((memptr)&digi->data);
}

void
SD_PlayDigitized(word which,int16_t leftpos,int16_t rightpos)
{
	word	len;
	memptr	addr;

	if (!DigiMode)
		return;

	SD_StopDigitized();
	if (which >= NumDigi)
	{
//		SD_ERROR(SD_PLAYDIGITIZED_BAD_SOUND);
		which = 1;
		return;
	}

	SD_SetPosition(leftpos,rightpos);

	//BE_ST_PlayDigiSound(which);
	sample_t *digi = &g_digiSounds[which];

	if (!digi->data)
	{
		//printf("%s reloading digi sound %d...\n", __FUNCTION__, which);
		SDL_LoadDigiSound(which);
	}

	MM_SetLock((memptr)&digi->data, true);
	BEL_ST_LoadSample(digi->data, digi->length, SOUND_DIGI);
	BEL_ST_PlaySample(CHANNEL_DIGI, SOUND_DIGI, digi->length, digi->rate, digivol, digipan, false);

	SD_Poll();
}

void
SD_SetDigiDevice(SDSMode mode)
{
	if (mode == DigiMode)
		return;

	SD_StopDigitized();

	DigiMode = mode;

	if (mode == sds_Off)
	{
		for (int i = 0; i < NumDigi; i++)
		{
			SDL_FreeDigiSound(i);
		}
	}
	else
	{
		//printf("NumDigi %d\n", NumDigi);
		//PM_UnlockMainMem();
		// precache the digi sound effects
		for (int i = 0; i < NumDigi; i++)
		{
			SDL_LoadDigiSound(i);
		}
		// free the adlib sounds with digi replacements
		for (int i = 0; i < LASTSOUND; i++)
		{
			//printf("%s DigiMap[%d] = %d\n", __FUNCTION__, i, DigiMap[i]);
			if (DigiMap[i] != -1)
				BEL_ST_FreeSound(&g_adlibSounds[i]);
		}
		//PM_CheckMainMem();
	}
}

static void
SDL_SetupDigi(void)
{
	memptr	list;
	word	far *p,
			pg;
	int16_t		i;

	PM_UnlockMainMem();
	MM_GetPtr(&list,PMPageSize);
	PM_CheckMainMem();
	//p = (word far *)MK_FP(PM_GetPage(ChunksInFile - 1),0);
	p = (word far *)PM_GetPage(ChunksInFile - 1);
	_fmemcpy((void far *)list,(void far *)p,PMPageSize);
	pg = PMSoundStart;
	for (i = 0;i < PMPageSize / (sizeof(word) * 2);i++,p += 2)
	{
		if (pg >= ChunksInFile - 1)
			break;
/*#ifdef __AMIGA__
		pg += (SWAP16LE(p[1]) + (PMPageSize - 1)) / PMPageSize;
#else*/
		pg += (p[1] + (PMPageSize - 1)) / PMPageSize;
//#endif
	}
	PM_UnlockMainMem();
	MM_GetPtr((memptr *)&DigiList,i * sizeof(word) * 2);
	_fmemcpy((void far *)DigiList,(void far *)list,i * sizeof(word) * 2);
	MM_FreePtr(&list);
	NumDigi = i;
/*#ifdef __AMIGA__
	for (i = 0; i < NumDigi * 2; i++)
	{
		DigiList[i] = SWAP16LE(DigiList[i]);
	}
#endif*/

	for (i = 0;i < LASTSOUND;i++)
		DigiMap[i] = -1;
}

// 	AdLib Code

///////////////////////////////////////////////////////////////////////////
//
//	alOut(n,b) - Puts b in AdLib card register n
//
///////////////////////////////////////////////////////////////////////////

void
alOut(byte n,byte b)
{
	// nothing to do here...
}

///////////////////////////////////////////////////////////////////////////
//
//	SDL_t0Service() - My timer 0 ISR which handles the different timings and
//		dispatches to whatever other routines are appropriate
//
///////////////////////////////////////////////////////////////////////////
static void
SDL_t0Service(REG(a1, APTR intData))
{
	TimeCount++;
	if (!sqPlayedOnce)
		sqPlayedOnce = (channelFlags[CHANNEL_MUSIC] & FLAG_PLAYEDONCE) != 0;

}

////////////////////////////////////////////////////////////////////////////
//
//	SDL_ShutDevice() - turns off whatever device was being used for sound fx
//
////////////////////////////////////////////////////////////////////////////
static void
SDL_ShutDevice(void)
{
	if (SoundMode == sdm_Off)
		return;

	for (int i = 0; i < LASTSOUND; i++)
	{
		BEL_ST_FreeSound(&g_adlibSounds[i]);
	}
	SoundMode = sdm_Off;
}

///////////////////////////////////////////////////////////////////////////
//
//	SDL_StartDevice() - turns on whatever device is to be used for sound fx
//
///////////////////////////////////////////////////////////////////////////
static void
SDL_StartDevice(void)
{
	if (SoundMode == sdm_Off)
		return;

	//PM_UnlockMainMem();
	for (int i = 0; i < LASTSOUND; i++)
	{
		// skip sounds with digi replacements
		if ((DigiMode != sds_Off) && (DigiMap[i] != -1))
			continue;
		// skip unused sounds
#ifdef GAMEVER_RESTORATION_AOG
		if (i == 20 || i == 51)
#else
		if (i == 51 || i == 77 || i == 93)
#endif
			continue;

		BEL_ST_LoadSound(i);
	}
	//PM_CheckMainMem();
	SoundNumber = SoundPriority = 0;
}

static void SDL_SoundFinished()
{
	if (SoundNumber)
	{
		sample_t *digi = &g_adlibSounds[SoundNumber];
		MM_SetLock((memptr)&digi->data, false);
	}
	SoundNumber = SoundPriority = 0;
}

//	Public routines

///////////////////////////////////////////////////////////////////////////
//
//	SD_SetSoundMode() - Sets which sound hardware to use for sound effects
//
///////////////////////////////////////////////////////////////////////////
boolean
SD_SetSoundMode(SDMode mode)
{
	boolean	result = false;
	word	tableoffset;

	SD_StopSound();

	if ((mode == sdm_AdLib) && !AdLibPresent)
		mode = sdm_Off;

	switch (mode)
	{
	case sdm_Off:
		tableoffset = STARTPCSOUNDS;
		NeedsDigitized = false;
		result = true;
		break;
	case sdm_AdLib:
		if (AdLibPresent)
		{
			tableoffset = STARTADLIBSOUNDS;
			NeedsDigitized = false;
			result = true;
		}
		break;
	}

	if (result && (mode != SoundMode))
	{
		SDL_ShutDevice();
		SoundMode = mode;
		SoundTable = (SoundCommon **)&audiosegs[tableoffset];
		SDL_StartDevice();
	}

	return(result);
}

///////////////////////////////////////////////////////////////////////////
//
//	SD_SetMusicMode() - sets the device to use for background music
//
///////////////////////////////////////////////////////////////////////////
boolean
SD_SetMusicMode(SMMode mode)
{
	boolean	result = false;

	SD_MusicOff();

	switch (mode)
	{
	case smm_Off:
		NeedsMusic = false;
		result = true;
		BEL_ST_FreeMusic();
		break;
	case smm_AdLib:
		if (AdLibPresent)
		{
			NeedsMusic = true;
			result = true;
		}
		break;
	}

	if (result)
		MusicMode = mode;

	return(result);
}

///////////////////////////////////////////////////////////////////////////
//
//	SD_Startup() - starts up the Sound Mgr
//		Detects all additional sound hardware and installs my ISR
//
///////////////////////////////////////////////////////////////////////////
void
SD_Startup(void)
{
	int16_t	i;

	if (SD_Started)
		return;

	SoundUserHook = 0;

	TimeCount = 0;

	SD_SetSoundMode(sdm_Off);
	SD_SetMusicMode(smm_Off);

	SoundSourcePresent = false;
	AdLibPresent = true;
	SoundBlasterPresent = true;
	SBProPresent = true;

	if (!BE_ST_InitAudio())
	{
		AdLibPresent = false;
		SoundBlasterPresent = false;
		SBProPresent = false;
	}

	SDL_SetupDigi();

	g_timerIntHandle = AddTimerInt((APTR)SDL_t0Service, NULL);
	ULONG timerDelay = (1000 * 1000) / TickBase;
	StartTimerInt(g_timerIntHandle, timerDelay, TRUE);

	SD_Started = true;
}

///////////////////////////////////////////////////////////////////////////
//
//	SD_Default() - Sets up the default behaviour for the Sound Mgr whether
//		the config file was present or not.
//
///////////////////////////////////////////////////////////////////////////
void
SD_Default(boolean gotit,SDMode sd,SMMode sm)
{
	boolean	gotsd,gotsm;

	gotsd = gotsm = gotit;

	if (gotsd)	// Make sure requested sound hardware is available
	{
		switch (sd)
		{
		case sdm_AdLib:
			gotsd = AdLibPresent;
			break;
		}
	}
	if (!gotsd)
	{
		if (AdLibPresent)
			sd = sdm_AdLib;
		else
			sd = sdm_PC;
	}
	if (sd != SoundMode)
		SD_SetSoundMode(sd);


	if (gotsm)	// Make sure requested music hardware is available
	{
		switch (sm)
		{
		case sdm_AdLib:
			gotsm = AdLibPresent;
			break;
		}
	}
	if (!gotsm)
	{
		if (AdLibPresent)
			sm = smm_AdLib;
	}
	if (sm != MusicMode)
		SD_SetMusicMode(sm);
}

///////////////////////////////////////////////////////////////////////////
//
//	SD_Shutdown() - shuts down the Sound Mgr
//		Removes sound ISR and turns off whatever sound hardware was active
//
///////////////////////////////////////////////////////////////////////////
void
SD_Shutdown(void)
{
	if (!SD_Started)
		return;

	SD_MusicOff();
	BEL_ST_FreeMusic();
	SD_StopSound();
	SDL_ShutDevice();
	SD_SetDigiDevice(sds_Off);
	//SDL_CleanDevice();
	BE_ST_ShutdownAudio();

	StopTimerInt(g_timerIntHandle);
	RemTimerInt(g_timerIntHandle);

	SD_Started = false;
}

///////////////////////////////////////////////////////////////////////////
//
//	SD_SetUserHook() - sets the routine that the Sound Mgr calls every 1/70th
//		of a second from its timer 0 ISR
//
///////////////////////////////////////////////////////////////////////////
void
SD_SetUserHook(void (* hook)(void))
{
	SoundUserHook = hook;
}

///////////////////////////////////////////////////////////////////////////
//
//	SD_PositionSound() - Sets up a stereo imaging location for the next
//		sound to be played. Each channel ranges from 0 to 15.
//
///////////////////////////////////////////////////////////////////////////
void
SD_PositionSound(int16_t leftvol,int16_t rightvol)
{
	LeftPosition = leftvol;
	RightPosition = rightvol;
	nextsoundpos = true;
}

///////////////////////////////////////////////////////////////////////////
//
//	SD_PlaySound() - plays the specified sound on the appropriate hardware
//
///////////////////////////////////////////////////////////////////////////
boolean
SD_PlaySound(soundnames sound)
{
	boolean		ispos;
	SoundCommon	far *s;
	int16_t	lp,rp;

	lp = LeftPosition;
	rp = RightPosition;
	LeftPosition = 0;
	RightPosition = 0;

	ispos = nextsoundpos;
	nextsoundpos = false;

	if (sound == -1)
		return(false);

	//s = (void *)MK_FP((size_t)SoundTable[sound],0);
	s = (SoundCommon *)(SoundTable[sound]);
	if ((SoundMode != sdm_Off) && !s)
		SD_ERROR(SD_PLAYSOUND_UNCACHED);

	if ((DigiMode != sds_Off) && (DigiMap[sound] != -1))
	{
		if (DigiPriority && !DigiNumber)
			SD_ERROR(SD_PLAYSOUND_PRI_NO_SOUND);

		if (s->priority < DigiPriority)
			return(false);

		SD_PlayDigitized(DigiMap[sound],lp,rp);
		SoundPositioned = ispos;
		DigiNumber = sound;
		DigiPriority = s->priority;

		return(true);
	}
	//printf("%s sound %d priority %d playing %d\n", __FUNCTION__, __LINE__, SoundNumber, SoundPriority, BE_ST_SoundPlaying());
	if (SoundPriority && !(channelFlags[CHANNEL_ADLIB] & FLAG_ISPLAYING))
	{
		//printf("%s sound %d priority %d finished\n", __FUNCTION__, SoundNumber, SoundPriority);
		SDL_SoundFinished();
	}
	//printf("%s:%d length %d priority %d SoundPriority %d\n", __FUNCTION__, __LINE__, s->length, s->priority, SoundPriority);
	if (SoundMode == sdm_Off)
		return(false);
	if (!s->length)
		SD_ERROR(SD_PLAYSOUND_ZERO_LEN);
	if (s->priority < SoundPriority)
		return(false);

	if (SoundMode != sdm_Off)
	{
		sample_t *digi = &g_adlibSounds[sound];

		if (!digi->data)
		{
			//printf("%s reloading sound %d...\n", __FUNCTION__, sound);
			BEL_ST_LoadSound(sound);
			if (!digi->data)
				return false;
		}

		MM_SetLock((memptr)&digi->data, true);
		BEL_ST_LoadSample(digi->data, digi->length, SOUND_ADLIB);
		BEL_ST_PlaySample(CHANNEL_ADLIB, SOUND_ADLIB, digi->length, digi->rate, /*0xFFF0*/0xC000, 0x8000, false);
	}

	SoundNumber = sound;
	SoundPriority = s->priority;

	return(false);
}

///////////////////////////////////////////////////////////////////////////
//
//	SD_SoundPlaying() - returns the sound number that's playing, or 0 if
//		no sound is playing
//
///////////////////////////////////////////////////////////////////////////
word
SD_SoundPlaying(void)
{
	if (SoundMode == sdm_Off || !(channelFlags[CHANNEL_ADLIB] & FLAG_ISPLAYING))
		return(false);

	return(SoundNumber);
}

///////////////////////////////////////////////////////////////////////////
//
//	SD_StopSound() - if a sound is playing, stops it
//
///////////////////////////////////////////////////////////////////////////
void
SD_StopSound(void)
{
	if (DigiPlaying)
		SD_StopDigitized();

	if (SoundMode != sdm_Off)
		BEL_ST_StopSample(CHANNEL_ADLIB);

	SoundPositioned = false;
	SDL_SoundFinished();
}

///////////////////////////////////////////////////////////////////////////
//
//	SD_WaitSoundDone() - waits until the current sound is done playing
//
///////////////////////////////////////////////////////////////////////////
void
SD_WaitSoundDone(void)
{
	while (SD_SoundPlaying())
		BE_ST_ShortSleep();
}

///////////////////////////////////////////////////////////////////////////
//
//	SD_MusicOn() - turns on the sequencer
//
///////////////////////////////////////////////////////////////////////////
void
SD_MusicOn(void)
{
	sqActive = true;

	if (MusicMode == smm_Off)
		return;

	if (!g_adlibSounds[LASTSOUND].data)
		return;

	BEL_ST_PlaySample(CHANNEL_MUSIC, SOUND_MUSIC, g_adlibSounds[LASTSOUND].length, g_adlibSounds[LASTSOUND].rate, 0x10000, 0x8000, true);
}

///////////////////////////////////////////////////////////////////////////
//
//	SD_MusicOff() - turns off the sequencer and any playing notes
//
///////////////////////////////////////////////////////////////////////////
void
SD_MusicOff(void)
{
	sqActive = false;

	if (MusicMode == smm_Off)
		return;

	BEL_ST_StopSample(CHANNEL_MUSIC);
}

///////////////////////////////////////////////////////////////////////////
//
//	SD_StartMusic() - starts playing the music pointed to
//
///////////////////////////////////////////////////////////////////////////
void
SD_StartMusic(MusicGroup far *music)
{
	SD_MusicOff();

	sqPlayedOnce = false;

	if (MusicMode == smm_AdLib)
	{
		//printf("%s music %d\n", __FUNCTION__, (int)music);
		//PM_UnlockMainMem();
		BEL_ST_FreeMusic();
		BEL_ST_LoadMusic((int)music);
		SD_MusicOn();
		//PM_CheckMainMem();
	}
	else
		sqPlayedOnce = true;

}

///////////////////////////////////////////////////////////////////////////
//
//	SD_FadeOutMusic() - starts fading out the music. Call SD_MusicPlaying()
//		to see if the fadeout is complete
//
///////////////////////////////////////////////////////////////////////////
void
SD_FadeOutMusic(void)
{
	if (MusicMode == smm_Off)
		return;

	SD_MusicOff();
	BEL_ST_FreeMusic();
}
