#include <stdio.h>
#include <stdint.h>
#include <math.h>

typedef int32_t fixed;

#define FINEANGLES      3600
#define PI      			3.141592657
#define GLOBAL1         (1l<<16)
#define TILEGLOBAL  		GLOBAL1
#define ANGLES          360                                     // must be divisable by 4
#define ANGLEQUAD       (ANGLES/4)
#define TANTABLESIZE ANGLES+1
#define MINDIST         (0x5800l)
#define MAXVIEWWIDTH    320
#define FOCALLENGTH     (0x5700l)               // in global coordinates
#define VIEWGLOBAL      0x10000                 // globals visable flush to wall

int32_t			finetangent[FINEANGLES/4];
fixed			sintable[ANGLES+ANGLES/4];
int16_t			tantoangle[TANTABLESIZE];
int16_t			pixeltang[MAXVIEWWIDTH/2];

const   float   radtoint = (float)FINEANGLES/2/PI;

void BuildTables (void)
{
  int           i;
  float         angle,anglestep;
  double        tang;
  fixed         value;


//
// calculate fine tangents
//

	for (i=0;i<FINEANGLES/8;i++)
	{
		tang = tan( (i+0.5)/radtoint);
		finetangent[i] = tang*TILEGLOBAL;
		finetangent[FINEANGLES/4-1-i] = 1/tang*TILEGLOBAL;
	}

//
// costable overlays sintable with a quarter phase shift
// ANGLES is assumed to be divisable by four
//
// The low word of the value is the fraction, the high bit is the sign bit,
// bits 16-30 should be 0
//

  angle = 0;
  anglestep = PI/2/ANGLEQUAD;
  for (i=0;i<=ANGLEQUAD;i++)
  {
	value=GLOBAL1*sin(angle);
	sintable[i]=
	  sintable[i+ANGLES]=
	  sintable[ANGLES/2-i] = value;
	sintable[ANGLES-i]=
	  sintable[ANGLES/2+i] = value | 0x80000000l;
	angle += anglestep;
  }

	for (i = 0; i < TANTABLESIZE; i++)
	{
		angle = atan((float)i/(TANTABLESIZE-1)) * radtoint;
		tantoangle[i] = (int)angle;
	}
	for (i = 0; i < MAXVIEWWIDTH/2; i++)
	{
		tang = (int32_t)i*VIEWGLOBAL/MAXVIEWWIDTH/((double)FOCALLENGTH+MINDIST);
		angle = atan(tang) * radtoint;
		pixeltang[i] = (int)angle;
	}
}

int main(void)
{
	FILE *fp;
	int i;

	BuildTables();

	fp = fopen("tables.h", "w");
	if (!fp)
	{
		return 1;
	}

	fprintf(fp, "int32_t far finetangent[FINEANGLES/4] = {\n");
	for (i=0;i<FINEANGLES/4;i++)
	{
		if (i > 0) fprintf(fp, ",\n");
		fprintf(fp, "\t%d", finetangent[i]);
	}
	fprintf(fp, "};\n\n");

	fprintf(fp, "fixed far sintable[ANGLES+ANGLES/4] = {\n");
	for (i=0;i<ANGLES+ANGLES/4;i++)
	{
		if (i > 0) fprintf(fp, ",\n");
		fprintf(fp, "\t%d", sintable[i]);
	}
	fprintf(fp, "};\nfixed far *costable = sintable+(ANGLES/4);\n\n");

	fprintf(fp, "int16_t tantoangle[TANTABLESIZE] = {\n");
	for (i=0;i<TANTABLESIZE;i++)
	{
		if (i > 0) fprintf(fp, ",\n");
		fprintf(fp, "\t%d", tantoangle[i]);
	}
	fprintf(fp, "};\n\n");
	
	fprintf(fp, "int16_t pixeltang[MAXVIEWWIDTH/2] = {\n");
	for (i=0;i<MAXVIEWWIDTH/2;i++)
	{
		if (i > 0) fprintf(fp, ",\n");
		fprintf(fp, "\t%d", pixeltang[i]);
	}
	fprintf(fp, "};\n");

	fclose(fp);

	return 0;
}
