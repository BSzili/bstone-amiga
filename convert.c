#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
//#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>

#include "id_heads.h"
#include "jm_io.h"

#if 0
// this matches the value used in SDL_SetIntsPerSec
#define PC_PIT_RATE 1192030
#else
#define PC_PIT_RATE 1193182
#endif
static uint32_t g_sampleRate = 11025;
static uint32_t g_hackLength;
extern word sqHackLen,sqHackSeqLen;
static boolean useNukedOPL = false;
#define CONVERT_MUSIC

static void (*g_sdlCallbackSDFuncPtr)(void) = 0;

// sound related
static uint32_t g_sdlSampleOffsetInSound, g_sdlSamplePerPart;

// refkeen compatibility
//#define BE_Cross_LogMessage(x, fmt, ...) fprintf(stderr, (fmt), __VA_ARGS__); 
#define BE_Cross_LogMessage(x, fmt, ...) 
#define BE_Cross_TypedMin(T, x, y) ({T _x = (x), _y = (y); (_x < _y) ? _x : _y;})


// id_ca.c

void OpenAudioFile(void);
void CloseAudioFile(void);
void CA_CannotOpen(char *string);

// be_st_sdl_audio_timer.c

void BE_ST_ShortSleep(void)
{
}

void BE_ST_SetTimer(uint16_t speed)
{
#if 1
	// try to do some rounding here
	//g_sdlSamplePerPart = (uint32_t)floorf((float)speed * g_sampleRate / PC_PIT_RATE + 0.5f);
	g_sdlSamplePerPart = (uint32_t)(((((int64_t)(speed * g_sampleRate) << 16) / PC_PIT_RATE) + (1 << 15)) >> 16);
#else
	g_sdlSamplePerPart = (int32_t)speed * g_sampleRate / PC_PIT_RATE;
#endif
	//printf("%s speed %u g_sdlSamplePerPart %d\n", __FUNCTION__, speed, g_sdlSamplePerPart);
}

void BE_ST_StartAudioSDService(void (*funcPtr)(void))
{
	g_sdlCallbackSDFuncPtr = funcPtr;
}

void BE_ST_StopAudioSDService(void)
{
	g_sdlCallbackSDFuncPtr = 0;
}

// 3d_main.c

void ShutdownId (void)
{
	SD_Shutdown ();
	//PM_Shutdown ();
	//CA_Shutdown ();
	CloseAudioFile();
}


/*******************************************************************************
OPL emulation, powered by dbopl from DOSBox and using bits of code from Wolf4SDL
*******************************************************************************/

#ifdef NUKED_OPL
#include "nukedopl/opl3.h"
#endif
#ifdef DOSBOX_OPL
#include "opl/dbopl.h"
#else
#define BOOL FMBOOL
#include "fmopl.h"
#endif

//#define OPL_NUM_OF_SAMPLES 2048 // About 40ms of OPL sound data
#define OPL_NUM_OF_SAMPLES 1000
//#define OPL_SAMPLE_RATE 49716
// Use this if the audio subsystem is disabled for most (we want a BYTES rate of 1000Hz, same units as used in values returned by SDL_GetTicks())
#define NUM_OF_BYTES_FOR_SOUND_CALLBACK_WITH_DISABLED_SUBSYSTEM 1000

/*
static int8_t g_sdlALOutSamples[OPL_NUM_OF_SAMPLES];
static uint32_t g_sdlALOutSamplesEnd = 0;
*/
static int numreadysamples = 0;

#if defined(NUKED_OPL) || defined(DOSBOX_OPL)
typedef int16_t INT16;

#ifdef NUKED_OPL
static opl3_chip oplChipNuked;
#endif
static Chip oplChip;

static inline int YM3812Init(int numChips, int clock, int rate)
{
#ifdef NUKED_OPL
	OPL3_Reset(&oplChipNuked, rate);
#endif
	DBOPL_InitTables();
	Chip__Chip(&oplChip);
	Chip__Setup(&oplChip, rate);
	return 0;
}

static inline void YM3812Write(int which, uint16_t reg, uint8_t val)
{
#ifdef NUKED_OPL
	if (useNukedOPL)
		OPL3_WriteRegBuffered(&oplChipNuked, reg, val);
	else
#endif
	Chip__WriteReg(&oplChip, reg, val);
}

static inline void YM3812UpdateOne(int which, INT16 *outbuf, int length)
{
#ifdef NUKED_OPL
	static int16_t buffer16[OPL_NUM_OF_SAMPLES * 2];
#endif
	static Bit32s buffer[OPL_NUM_OF_SAMPLES];
	int i;

	// length should be at least the max. samplesPerMusicTick
	// in Catacomb 3-D and Keen 4-6, which is param_samplerate / 700.
	// So 512 is sufficient for a sample rate of 358.4 kHz.
	if(length > OPL_NUM_OF_SAMPLES)
		length = OPL_NUM_OF_SAMPLES;

#ifdef NUKED_OPL
	if (useNukedOPL)
		OPL3_GenerateStream(&oplChipNuked, buffer16, length);
	else
#endif
	Chip__GenerateBlock2(&oplChip, length, buffer);


	// Output is 16-bit stereo sound
	uint8_t *stream = (uint8_t *)outbuf;
	for(i = 0; i < length; i++)
	{
		// Scale volume
		int16_t sample;
#ifdef NUKED_OPL
		if (useNukedOPL)
			sample = buffer16[2*i];
		else
#endif
		sample = buffer[i];

		sample >>= 6;
		//if (sample > 127 || sample < -128) printf("%s clipping warning %d %d\n", __FUNCTION__, sample, length);
		if(sample > 127) sample = 127;
		else if(sample < -128) sample = -128;
		stream[i] = sample;
		//printf("stream[%d] = %d\n", i, stream[i]);
	}
}
#endif

void BE_ST_ALOut(uint8_t reg,uint8_t val)
{
	// FIXME: The original code for alOut adds 6 reads of the
	// register port after writing to it (3.3 microseconds), and
	// then 35 more reads of register port after writing to the
	// data port (23 microseconds).
	//
	// It is apparently important for a portion of the fuse
	// breakage sound at the least. For now a hack is implied.
	YM3812Write(0, reg, val);

	// Hack comes with a "magic number"
	// that appears to make it work better
	/*
	unsigned int length = g_hackLength;

	if (length > OPL_NUM_OF_SAMPLES - g_sdlALOutSamplesEnd)
	{
		BE_Cross_LogMessage(BE_LOG_MSG_WARNING, "BE_ST_ALOut overflow, want %u, have %u\n", length, OPL_NUM_OF_SAMPLES - g_sdlALOutSamplesEnd); // FIXME - Other thread
		length = OPL_NUM_OF_SAMPLES - g_sdlALOutSamplesEnd;
	}
	if (length)
	{
		YM3812UpdateOne(&g_sdlALOutSamples[g_sdlALOutSamplesEnd], length);
		g_sdlALOutSamplesEnd += length;
	}
	*/
}

static void BEL_ST_Simple_CallBack(void *unused, uint8_t *stream, int len)
{
#if 1
    int sampleslen = len;

    while(1)
    {
        if(numreadysamples)
        {
            if(numreadysamples<sampleslen)
            {
                YM3812UpdateOne(0, (INT16 *)stream, numreadysamples);
                stream += numreadysamples;
                sampleslen -= numreadysamples;
            }
            else
            {
                YM3812UpdateOne(0, (INT16 *)stream, sampleslen);
                numreadysamples -= sampleslen;
                return;
            }
        }
        g_sdlCallbackSDFuncPtr();
        numreadysamples = g_sdlSamplePerPart;
    }
#else
	int8_t *currSamplePtr = (int8_t *)stream;
	uint32_t currNumOfSamples;
	//memset(stream, 0, len);

	while (len)
	{
		if (!g_sdlSampleOffsetInSound)
		{
			// FUNCTION VARIABLE (We should use this and we want to kind-of separate what we have here from original code.)
			g_sdlCallbackSDFuncPtr();
		}
		// Now generate sound
		currNumOfSamples = BE_Cross_TypedMin(uint32_t, len, g_sdlSamplePerPart-g_sdlSampleOffsetInSound);
		/*** AdLib (including hack for alOut delays) ***/
		//if (g_sdlEmulatedOPLChipReady)
		{
			// We may have pending AL data ready, but probably less than required
			// for filling the stream buffer, so generate some silence.
			//
			// Make sure we don't overthrow the AL buffer, though.
			uint32_t targetALSamples = currNumOfSamples;
			if (targetALSamples > OPL_NUM_OF_SAMPLES)
			{
				BE_Cross_LogMessage(BE_LOG_MSG_WARNING, "BEL_ST_Simple_CallBack AL overflow, want %u, have %u\n", targetALSamples, (unsigned int)OPL_NUM_OF_SAMPLES); // FIXME - Other thread
				targetALSamples = OPL_NUM_OF_SAMPLES;
			}
			// TODO Output overflow warning if there's any
			if (targetALSamples > g_sdlALOutSamplesEnd)
			{
				YM3812UpdateOne(&g_sdlALOutSamples[g_sdlALOutSamplesEnd], targetALSamples - g_sdlALOutSamplesEnd);
				g_sdlALOutSamplesEnd = targetALSamples;
			}
			// Mix with AL data
			for (uint32_t i = 0; i < targetALSamples; ++i)
				//currSamplePtr[i] = (currSamplePtr[i] + g_sdlALOutSamples[i]) / 2;
				currSamplePtr[i] = g_sdlALOutSamples[i];
			// Move pending AL data
			if (targetALSamples < g_sdlALOutSamplesEnd)
				memmove(g_sdlALOutSamples, &g_sdlALOutSamples[targetALSamples], g_sdlALOutSamplesEnd - targetALSamples);
			g_sdlALOutSamplesEnd -= targetALSamples;
		}
		// We're done for now
		currSamplePtr += currNumOfSamples;
		g_sdlSampleOffsetInSound += currNumOfSamples;
		len -= currNumOfSamples;
		// End of part?
		if (g_sdlSampleOffsetInSound >= g_sdlSamplePerPart)
		{
			g_sdlSampleOffsetInSound = 0;
		}
	}
#endif
}

typedef struct
{
	uint32_t magic; /* magic number */
	uint32_t hdr_size; /* size of this header */ 
	uint32_t data_size; /* length of data (optional) */ 
	uint32_t encoding; /* data encoding format */
	uint32_t sample_rate; /* samples per second */
	uint32_t channels; /* number of interleaved channels */
} Audio_filehdr;

#define AUDIO_FILE_MAGIC ((uint32_t)0x2e736e64) /* Define the magic number */  
#define AUDIO_FILE_ENCODING_LINEAR_8 (2) /* 8-bit linear PCM */

uint8_t buff[NUM_OF_BYTES_FOR_SOUND_CALLBACK_WITH_DISABLED_SUBSYSTEM];

static void write_header(FILE *fd)
{
	Audio_filehdr header;
	uint32_t size;
	//size = ftell(fd);
	size = 0xffffffff;
	header.magic = SWAP32BE(AUDIO_FILE_MAGIC);
	header.hdr_size = SWAP32BE(sizeof(header));
	header.data_size = SWAP32BE(size);
	header.encoding = SWAP32BE(AUDIO_FILE_ENCODING_LINEAR_8);
	header.sample_rate = SWAP32BE(g_sampleRate);
	header.channels = SWAP32BE((uint32_t)1);
	//fseek(fd, 0, SEEK_SET);
	fwrite(&header, sizeof(header), 1, fd);
}

// converter entry point
int _argc;
char **_argv;
int main(int argc, char **argv)
{
	char filename[256];
	FILE *fd;

	_argc = argc;
	_argv = argv;

	//setbuf(stdout, NULL);

	if (argc >= 2)
	{
		g_sampleRate = atoi(argv[1]);
	}
	printf("Conversion init, sample rate: %d Hz\n", g_sampleRate);
	YM3812Init(1, 3579545, g_sampleRate);
	//g_hackLength = g_sampleRate / 10000;

	printf("Conversion startup...\n");
	CheckForEpisodes();
	//PM_Startup ();
	SD_Startup ();
	//CA_Startup ();
	CAL_SetupAudioFile();
	atexit(ShutdownId);

	printf("Loading sounds...\n");
	SD_SetSoundMode(sdm_AdLib);
	CA_LoadAllSounds();

	//printf("starting conversion\n");

	mkdir("adlib", 0755); 

	// play them
	useNukedOPL = true;
	for (int i = 0; i < LASTSOUND; i++)
	{
		printf("Converting sound %d/%d", i, LASTSOUND-1);
		fflush(stdout);

#ifdef GAMEVER_RESTORATION_AOG
		if (i == 20 || i == 51)
#else
		if (i == 51 || i == 77 || i == 93)
#endif
		{
			puts(" - unused sound");
			continue;
		}

		snprintf(filename, sizeof(filename), "adlib/sound%02d.au", i);
		fd = fopen(filename, "r");
		if (fd)
		{
			// already exists, skip this one
			fclose(fd);
			puts(" - already exists");
			continue;
		}

		SD_SetSoundMode(sdm_AdLib);
		SD_PlaySound((soundnames)i);

		fd = fopen(filename, "w");
		write_header(fd);

		// render the sound data
		do
		{
			BEL_ST_Simple_CallBack(NULL, buff, sizeof(buff));
			putchar('.'); fflush(stdout);
			fwrite(buff, sizeof(buff), 1, fd);
		}
		while (SD_SoundPlaying());

		printf("\n");

		fclose(fd);

		// reset the emulator
		SD_SetSoundMode(sdm_Off);
	}
	useNukedOPL = false;

#ifdef CONVERT_MUSIC
	const char indicators[] = {'-', '\\', '|', '/'};
	const char convtext[] = "Converting music";
	SD_SetMusicMode(smm_AdLib);
	for (int i = 0; i < LASTMUSIC; i++)
	{
		printf("%s %d/%d", convtext, i, LASTMUSIC-1);

		snprintf(filename, sizeof(filename), "adlib/music%02d.au", i);
		fd = fopen(filename, "r");
		if (fd)
		{
			// already exists, skip this one
			fclose(fd);
			puts(" - already exists");
			continue;
		}

		CA_CacheAudioChunk(STARTMUSIC+i);
		SD_StartMusic((MusicGroup far *)audiosegs[STARTMUSIC+i]);

		fd = fopen(filename, "w");
		write_header(fd);

		//SD_MusicOn();
		// render the sound data
		time_t tstart = time(NULL);
		short count = 0;
		do
		{
			int percent = (sqHackSeqLen - sqHackLen) * 100 / sqHackSeqLen;
			BEL_ST_Simple_CallBack(NULL, buff, sizeof(buff));
			//printf("\nmusic playing %d playedonce %d", SD_MusicPlaying(), sqPlayedOnce);
			//putchar('.'); fflush(stdout);
			printf("\r%s %d/%d %3d%% [%c]", convtext, i, LASTMUSIC-1, percent, indicators[(count++) % sizeof(indicators)]);fflush(stdout);
			fwrite(buff, sizeof(buff), 1, fd);
		}
		while (/*SD_MusicPlaying() &&*/ !sqPlayedOnce);
		time_t tend = time(NULL);
		SD_MusicOff();
		MM_FreePtr ((memptr *)&audiosegs[STARTMUSIC + i]);

		//printf("\n");
		printf(" %ld secs\n", tend-tstart);

		fclose(fd);

		// reset the emulator
		SD_SetSoundMode(sdm_Off);
	}
#endif

	/*
	printf("Conversion shutdown...\n");
	SD_Shutdown ();
	//PM_Shutdown ();
	//CA_Shutdown ();
	CloseAudioFile();
	*/

	printf("Conversion finished!\n");
}

// id_mm.c

boolean		mmerror;
static char buffermem[BUFFERSIZE];
memptr		bufferseg = buffermem;
//mminfotype	mminfo;

/*void MM_Startup (void)
{
}

void MM_Shutdown (void)
{
}*/

void MM_GetPtr (memptr *baseptr,uint32_t size)
{
	*baseptr = malloc(size);
}

void MM_FreePtr (memptr *baseptr)
{
	free(*baseptr);
}

void MM_SetPurge (memptr *baseptr, int16_t purge)
{
}

void MM_SetLock (memptr *baseptr, boolean locked)
{
}

/*void MM_BombOnError (boolean bomb)
{
}*/

char QuitMsg[] = {"Unit: $%02x Error: $%02x"};
void Quit (char *error, ...)
{
	uint16_t unit,err;
	va_list ap;

	//ShutdownId ();
	if (error && *error)
	{
		//puts(error);
		va_start(ap,error);
		unit=va_arg(ap,int);
		err=va_arg(ap,int);
		printf(error,unit,err);
		printf("\n");
		va_end(ap);

		exit(1);
	}

	exit(0);
}

// jm_free.c

void CAL_SetupAudioFile (void)
{
	int handle;
	int32_t length;
	char fname[13];

	strcpy(fname,aheadname);
	strcat(fname,extension);

	if ((handle = open(fname,
		 O_RDONLY | O_BINARY, S_IREAD)) == -1)
		CA_CannotOpen(fname);

	length = filelength(handle);
	MM_GetPtr ((memptr *)&audiostarts,length);
	CA_FarRead(handle, (byte far *)audiostarts, length);
	close(handle);
#ifdef __AMIGA__
	for (int i = 0; i < length/sizeof(uint32_t); ++i)
	{
		audiostarts[i] = SWAP32LE(audiostarts[i]);
	}
#endif

	OpenAudioFile();
}

void CheckForEpisodes(void)
{
#ifndef GAMEVER_RESTORATION_AOG
	strcpy(extension,"VSI");
#else
	strcpy(extension,"BS6");
#endif
	strcat(audioname,extension);
}

// id_pm.c

	word				ChunksInFile;
	word				PMSpriteStart,PMSoundStart;

void PM_SetPageLock(int16_t pagenum,PMLockType lock) {}
void PM_SetMainMemPurge(int16_t level) {}
void PM_CheckMainMem(void) {}
memptr PM_GetPage(int16_t pagenum) { return NULL; }

// id_us_1.c

int16_t		US_CheckParm(char far *parm,char far * far * strings){ return(-1); }

// stubs

uint16_t bufferofs;
uint8_t vga_memory[1];
int16_t VW_MarkUpdateBlock (int16_t x1, int16_t y1, int16_t x2, int16_t y2) { return 0; }
