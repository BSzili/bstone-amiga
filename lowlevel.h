#ifndef BSZ_LOWLEVEL_H
#define BSZ_LOWLEVEL_H

#include <stdint.h>
//#include <stdbool.h>

void BE_ST_StartKeyboardService(void (*keyServicePtr)(void));
void BE_ST_StopKeyboardService(void);
int16_t BE_ST_KbHit(void);

void BE_ST_GetMouseDelta(int16_t *x, int16_t *y);
uint16_t BE_ST_GetMouseButtons(void);

void BE_ST_GetJoyAbs(uint16_t joy, uint16_t *xp, uint16_t *yp);
int16_t BE_ST_GetJoyButtons(uint16_t joy);

void BE_ST_ShortSleep(void);

#endif
