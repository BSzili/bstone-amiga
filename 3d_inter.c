// 3D_INTER.C

#include "3d_def.h"
//#pragma hdrstop


//==========================================================================
//
//									LOCAL CONSTANTS
//
//==========================================================================


//==========================================================================
//
//									LOCAL VARABLES
//
//==========================================================================

#ifndef ID_CACHE_BRIEFS
char BreifingText[13] = {"BRIEF_Wx.TXT"};
#endif

//==========================================================================

/*
==================
=
= CLearSplitVWB
=
==================
*/

void ClearSplitVWB (void)
{
	memset (update,0,sizeof(update));
	WindowX = 0;
	WindowY = 0;
	WindowW = 320;
	WindowH = 152;
}


//==========================================================================




/*
==================
=
= Breifing
=
==================
*/

boolean Breifing(breifing_type BreifingType,uint16_t episode)
{
#ifndef ID_CACHE_BRIEFS
	char chars[3] = {'L','W','I'};

	BreifingText[6] = chars[BreifingType];
	BreifingText[7] = '1'+episode;

	HelpPresenter(BreifingText,true,0,false);
#else
	HelpPresenter(NULL,true,BRIEF_W1+(episode*2)+BreifingType-1,false);
#endif

	return(EscPressed);
}


//==========================================================================


/*
=================
=
= PreloadGraphics
=
= Fill the cache up
=
=================
*/

void ShPrint(char far *text, int8_t shadow_color, boolean single_char)
{
	uint16_t old_color=fontcolor,old_x=px,old_y=py;
	char far *str,buf[2]={0,0};

	if (single_char)
	{
		str = buf;
		buf[0]=*text;
	}
	else
		str = text;

	fontcolor = shadow_color;
	py++;
	px++;
	USL_DrawString(str);						// JTR - This marks blocks!

	fontcolor = old_color;
	py = old_y;
	px = old_x;
	USL_DrawString(str);						// JTR - This marks blocks!
}

void PreloadUpdate(uint16_t current, uint16_t total)
{
	uint16_t w=WindowW-10;

	if (current > total)
		current=total;

	w = ((int32_t)w * current) / total;
	if (w)
		VWB_Bar(WindowX,WindowY,w-1,1,BORDER_TEXT_COLOR);

	VW_UpdateScreen();
}

char far prep_msg[]="^ST1^CEGet Ready, Blake!\r^XX";

void DisplayPrepingMsg(char far *text)
{
#if GAME_VERSION != SHAREWARE_VERSION

// Bomb out if FILE_ID.DIZ is bad!!
//
	if (((gamestate.mapon != 1) || (gamestate.episode != 0)) &&
		 (gamestate.flags & GS_BAD_DIZ_FILE))
		Quit(NULL);

#endif

// Cache-in font
//
	fontnumber=1;
	CA_CacheGrChunk(STARTFONT+1);
	BMAmsg(text);
	UNCACHEGRCHUNK(STARTFONT+1);

// Set thermometer boundaries
//
	WindowX = 36;
	WindowY = 188;
	WindowW = 260;
	WindowH = 32;


// Init MAP and GFX thermometer areas
//
	VWB_Bar(WindowX,WindowY-7,WindowW-10,2,BORDER_LO_COLOR);
	VWB_Bar(WindowX,WindowY-7,WindowW-11,1,BORDER_TEXT_COLOR-15);
	VWB_Bar(WindowX,WindowY,WindowW-10,2,BORDER_LO_COLOR);
	VWB_Bar(WindowX,WindowY,WindowW-11,1,BORDER_TEXT_COLOR-15);

// Update screen and fade in
//
	VW_UpdateScreen();
	if (screenfaded)
		VW_FadeIn();
}

#ifdef __AMIGA__
#define TEST(flags,mask) ((flags) & (mask))
#define SET(flags,mask) ((flags) |= (mask))
#define RESET(flags,mask) ((flags) &= ~(mask))

#define TEST_GOTPAGE(pagenum) (TEST(gotpage[(pagenum) >> 3], 1 << ((pagenum) & 7)))
#define RESET_GOTPAGE(pagenum) (RESET(gotpage[(pagenum) >> 3], 1 << ((pagenum) & 7)))
#define SET_GOTPAGE(pagenum) (SET(gotpage[(pagenum) >> 3], 1 << ((pagenum) & 7)))

#define PAGE_SIZE(chunks) (((chunks)+7)/8)

static byte gotpage[PAGE_SIZE(1335)]; // AOG 1152, PS 1335

/*
static boolean PreloadPage(int16_t pagenum)
{
	memptr pagebefore = PM_GetPageAddress(pagenum);
	memptr pageafter = PM_GetPage(pagenum);
	return pagebefore != pageafter;
}

static void PreloadSpriteRange(int16_t from, int16_t to)
{
	extern boolean PMThrashing;
	for (int16_t i = from; i <= to; i++)
	{
		PM_GetSpritePage(i);
		//printf("%s got sprite %d (%p) PMThrashing %d\n", __FUNCTION__, i, PM_GetPageAddress(PMSpriteStart + i), PMThrashing);
	}
}

static void PreloadSprite(int16_t picnum)
{
	extern boolean PMThrashing;
	//printf("%s sprite %d page %d (%p) PMThrashing %d\n", __FUNCTION__, picnum, PMSpriteStart + picnum, PM_GetPageAddress(PMSpriteStart + picnum), PMThrashing);
	PM_GetSpritePage(picnum);
}
*/
static void PreloadWall(int16_t pagenum)
{
	SET_GOTPAGE(pagenum);
}

static void PreloadSprite(int16_t picnum)
{
	SET_GOTPAGE(PMSpriteStart + picnum);
}

static void PreloadSpriteRange(int16_t from, int16_t to)
{
	for (int16_t i = from; i <= to; i++)
	{
		PreloadSprite(i);
	}
}

static void PreloadSound(soundnames sound)
{
	if ((DigiMode != sds_Off) && (DigiMap[sound] != -1))
	{
		int which = DigiMap[sound];
		word *soundInfoPage = (word *)PM_GetPage(ChunksInFile-1);
		word page = soundInfoPage[which * 2];
		SET_GOTPAGE(PMSoundStart + page);
	}
	else
	{
		// TODO this works but can result of spurious sound effect during loading
		SD_PlaySound(sound);
		SD_StopSound();
	}
}

static void PreloadWeapon(weapontype weapon);

static void PreloadBonus(stat_t itemnumber)
{
	switch (itemnumber)
	{
		case bo_clip2:
#ifdef GAMEVER_RESTORATION_AOG
			PreloadSprite(SPR_STAT_31);
#else
			PreloadSprite(SPR_STAT_26);
#endif
		// fallthrough
		case bo_clip:
#ifndef GAMEVER_RESTORATION_AOG
			// these can explode
			PreloadSpriteRange(SPR_CLIP_EXP1, SPR_CLIP_EXP8);
#endif
			PreloadSound(GETAMMOSND);
			break;
		case bo_pistol:
			PreloadSound(GETPISTOLSND);
			PreloadSprite(SPR_STAT_24);
			PreloadWeapon(wp_pistol);
			break;
		case bo_burst_rifle:
			PreloadSound(GETBURSTRIFLESND);
			PreloadSprite(SPR_STAT_27);
			PreloadWeapon(wp_burst_rifle);
			break;
		case bo_ion_cannon:
			PreloadSound(GETIONCANNONSND);
			PreloadSprite(SPR_STAT_28);
			PreloadWeapon(wp_ion_cannon);
			break;
		case bo_grenade:
			PreloadSound(GETCANNONSND);
			PreloadSprite(SPR_STAT_46);
			PreloadWeapon(wp_grenade);
			break;
#ifndef GAMEVER_RESTORATION_AOG
		case bo_bfg_cannon:
			PreloadSound(GETCANNONSND);
			PreloadSprite(SPR_STAT_34);
			PreloadWeapon(wp_bfg_cannon);
			break;
		case bo_plasma_detonator:
			PreloadSpriteRange(SPR_DOORBOMB, SPR_ALT_DOORBOMB);
			PreloadSound(GETDETONATORSND);
			break;
#endif

		case bo_red_key:
			PreloadSprite(SPR_STAT_32);
			PreloadSound(GETKEYSND);
			break;
		case bo_yellow_key:
			PreloadSprite(SPR_STAT_33);
			PreloadSound(GETKEYSND);
			break;
#ifdef GAMEVER_RESTORATION_AOG
		case bo_green_key:
			PreloadSound(GETKEYSND);
			break;
#endif
		case bo_blue_key:
			PreloadSprite(SPR_STAT_35);
			PreloadSound(GETKEYSND);
			break;
#ifdef GAMEVER_RESTORATION_AOG
		case bo_gold_key:
			PreloadSprite(SPR_STAT_36);
			PreloadSound(GETKEYSND);
			break;
#endif

		case bo_money_bag:
			PreloadSound(BONUS1SND);
			break;

		case bo_loot:
			PreloadSound(BONUS2SND);
			break;

		case bo_gold1:
		case bo_gold2:
		case bo_gold3:
		case bo_gold:
			PreloadSound(BONUS3SND);
			break;

		case bo_bonus:
			PreloadSound(BONUS4SND);
			break;

		case bo_fullheal:
			PreloadSound(HEALTH2SND);
			break;
		case bo_water_puddle:
		case bo_firstaid:
			PreloadSound(HEALTH1SND);
			break;
		case bo_ham:
			PreloadSpriteRange(SPR_STAT_44, SPR_STAT_45);
			PreloadSound(HEALTH1SND);
			break;
		case bo_chicken:
			PreloadSpriteRange(SPR_STAT_42, SPR_STAT_43);
			PreloadSound(HEALTH1SND);
			break;
		case bo_water:
			PreloadSpriteRange(SPR_STAT_40, SPR_STAT_41);
			PreloadSound(HEALTH1SND);
			break;
		case bo_candybar:
			PreloadSpriteRange(SPR_CANDY_BAR, SPR_CANDY_WRAPER);
			PreloadSound(HEALTH1SND);
			break;
		case bo_sandwich:
			PreloadSpriteRange(SPR_SANDWICH, SPR_SANDWICH_WRAPER);
			PreloadSound(HEALTH1SND);
			break;

		case bo_coin:
			PreloadSprite(SPR_STAT_77);
		// fallthrough
		case bo_coin5:
			PreloadSound(GOTTOKENSND);
		break;

#ifndef GAMEVER_RESTORATION_AOG
		case bo_automapper1:
			PreloadSound(RADAR_POWERUPSND);
			break;
#endif
	}
}

void PreloadObject(objtype *obj)
{
	switch (obj->obclass)
	{
		case deadobj:
			if (obj->flags & FL_OFFSET_STATES)
				PreloadSprite(obj->temp1+obj->state->shapenum);
			else
				PreloadSprite(obj->state->shapenum);
			break;
		case rentacopobj:
			PreloadSpriteRange(SPR_RENT_S_1, SPR_RENT_SHOOT3);
			PreloadSound(HALTSND);
			PreloadSound(RENTDEATH1SND);
			PreloadSound(RENTDEATH2SND);
#ifdef GAMEVER_RESTORATION_AOG
			PreloadSound(RENTDEATH3SND);
#endif
			if (!(gamestate.weapons &  (1<<wp_pistol)))
			{
				PreloadBonus(bo_pistol);
			}
			PreloadBonus(bo_coin);
			PreloadBonus(bo_clip2);
			break;
		case hang_terrotobj:
			PreloadSpriteRange(SPR_TERROT_1, SPR_TERROT_DEAD);
			PreloadSound(TURRETSND);
			PreloadSound(EXPLODE1SND);
			PreloadSound(EXPLODE2SND);
			break;
		case gen_scientistobj:
			PreloadSpriteRange(SPR_OFC_S_1, SPR_OFC_SHOOT3);
			if (obj->flags & FL_INFORMANT)
			{
				PreloadSound(INTERROGATESND);
				PreloadSound(INFORMANTDEATHSND);
				PreloadSound(INFORMDEATH2SND);
				PreloadSound(INFORMDEATH3SND);
			}
			else
			{
				PreloadSound(SCIENTISTHALTSND);
				PreloadSound(SCIENTISTDEATHSND);
				PreloadSound(SCIDEATH2SND);
				PreloadSound(SCIDEATH3SND);
			}
			PreloadBonus(bo_coin);
			PreloadBonus(bo_clip2);
			break;
		case podeggobj:
			PreloadSpriteRange(SPR_POD_EGG, SPR_POD_HATCH3);
			PreloadSound(PODHATCHSND);
		// fallthrough
		case podobj:
			PreloadSpriteRange(SPR_POD_WALK1, SPR_POD_SPIT3);
			PreloadSound(PODHALTSND);
			PreloadSound(CLAWATTACKSND);
			PreloadSound(PODDEATHSND);
		// fallthrough
		case podshotobj:
			PreloadSpriteRange(SPR_SPIT3_1, SPR_SPIT_EXP3_3);
			break;
		case electroobj:
			PreloadSpriteRange(SPR_ELEC_APPEAR1, SPR_ELEC_DIE3);
			PreloadSound(ELECTSHOTSND);
			PreloadSound(ELECAPPEARSND);
		// fallthrough
		case electroshotobj:
			PreloadSpriteRange(SPR_ELEC_SHOT1, SPR_ELEC_SHOT_EXP2);
			break;
		case electrosphereobj:
			PreloadSpriteRange(SPR_ELECTRO_SPHERE_ROAM1, SPR_ELECTRO_SPHERE_DIE4);
			PreloadSound(ELECARCDAMAGESND);
			break;
		case proguardobj:
			PreloadSpriteRange(SPR_PRO_S_1, SPR_PRO_SHOOT3);
			PreloadSound(PROHALTSND);
			PreloadSound(ATKBURSTRIFLESND);
			PreloadSound(PROGUARDDEATHSND);
			PreloadSound(PRODEATH2SND);
#ifdef GAMEVER_RESTORATION_AOG
			PreloadSound(PRODEATH3SND);
#endif
			if (!(gamestate.weapons & (1<<wp_burst_rifle)))
			{
				PreloadBonus(bo_burst_rifle);
			}
			PreloadBonus(bo_coin);
			PreloadBonus(bo_clip2);
			break;
		case genetic_guardobj:
			PreloadSpriteRange(SPR_GENETIC_W1, SPR_GENETIC_SHOOT3);
			PreloadSound(GGUARDHALTSND);
			PreloadSound(PUNCHATTACKSND);
			PreloadSound(GGUARDDEATHSND);
#ifdef GAMEVER_RESTORATION_AOG
			if (!(gamestate.weapons & (1<<wp_pistol)))
			{
				PreloadBonus(bo_pistol);
			}
			PreloadBonus(bo_clip2);
#endif
			break;
		case mutant_human1obj:
			PreloadSpriteRange(SPR_MUTHUM1_W1, SPR_MUTHUM1_SPIT3);
			PreloadSound(BLUEBOYHALTSND);
			PreloadSound(ELECTSHOTSND);
			PreloadSound(EXPLODE1SND);
			PreloadSound(EXPLODE2SND);
#ifdef GAMEVER_RESTORATION_AO
			PreloadBonus(bo_clip2);
#endif
		// fallthrough
		case mut_hum1shotobj:
			// TODO UNUSED?
			break;
#ifndef GAMEVER_RESTORATION_AOG
		case morphing_mutanthuman2obj:
			PreloadSpriteRange(SPR_MUTHUM2_MORPH1, SPR_MUTHUM2_MORPH9);
			PreloadSound(DOGBOYHALTSND);
		// fallthrough
#endif
		case mutant_human2obj:
			PreloadSpriteRange(SPR_MUTHUM2_W1, SPR_MUTHUM2_SPIT3);
			PreloadSound(DOGBOYHALTSND);
			PreloadSound(PUNCHATTACKSND);
			PreloadSound(DOGBOYDEATHSND);
			PreloadSound(ELECTSHOTSND);
			PreloadSound(SPITATTACKSND);

#ifndef GAMEVER_RESTORATION_AOG
			// uses electroshotobj
			PreloadSpriteRange(SPR_ELEC_SHOT1, SPR_ELEC_SHOT_EXP2);
#else
		// fallthrough
		case dogshotobj:
			PreloadSpriteRange(SPR_SPIT1_1, SPR_SPIT_EXP3_3);
#endif
			break;
		case lcan_wait_alienobj:
			PreloadSound(LCANBREAKSND);
		// fallthrough
		case lcan_alienobj:
			PreloadSpriteRange(SPR_LCAN_ALIEN_READY, SPR_LCAN_ALIEN_SPIT3);
			PreloadSound(LCANHALTSND);
			PreloadSound(CLAWATTACKSND);
			PreloadSound(LCANDEATHSND);
		// fallthrough
		case lcanshotobj:
			PreloadSpriteRange(SPR_SPIT3_1, SPR_SPIT_EXP3_3);
			break;
		case scan_wait_alienobj:
			PreloadSound(SCANBREAKSND);
		// fallthrough
		case scan_alienobj:
			PreloadSpriteRange(SPR_SCAN_ALIEN_READY, SPR_SCAN_ALIEN_SPIT3);
			PreloadSound(SCANHALTSND);
			PreloadSound(CLAWATTACKSND);
			PreloadSound(SPITATTACKSND);
			PreloadSound(SCANDEATHSND);
		// fallthrough
		case scanshotobj:
			PreloadSpriteRange(SPR_SPIT1_1, SPR_SPIT_EXP3_3);
			break;
		case gurney_waitobj:
		case gurneyobj:
			PreloadSpriteRange(SPR_GURNEY_MUT_READY, SPR_GURNEY_MUT_OUCH);
			PreloadSound(GURNEYSND);
			PreloadSound(GURNEYDEATHSND);
#ifdef GAMEVER_RESTORATION_AOG
			if (!(gamestate.weapons & (1<<wp_pistol)))
			{
				PreloadBonus(bo_pistol);
			}
			PreloadBonus(bo_clip2);
#endif
			break;
		case liquidobj:
			PreloadSpriteRange(SPR_LIQUID_M1, SPR_LIQUID_DEAD);
			PreloadSound(LIQUIDDIESND);
			// fallthrough
		case liquidshotobj:
			PreloadSpriteRange(SPR_LIQUID_SHOT_FLY_1, SPR_LIQUID_SHOT_BURST_3);
			break;
		case swatobj:
			PreloadSpriteRange(SPR_SWAT_S_1, SPR_SWAT_WOUNDED4);
			PreloadSound(SWATHALTSND);
			PreloadSound(ATKBURSTRIFLESND);
			PreloadSound(SWATDIESND);
			PreloadSound(SWATDEATH2SND);
#ifdef GAMEVER_RESTORATION_AOG
			PreloadSound(SWATDEATH3SND);
#endif
			if (!(gamestate.weapons &  (1<<wp_burst_rifle)))
			{
				PreloadBonus(bo_burst_rifle);
			}
			PreloadBonus(bo_coin);
			PreloadBonus(bo_clip2);
			break;
		case goldsternobj:
			PreloadSpriteRange(SPR_GOLD_S_1, SPR_GOLD_WARP5);
			PreloadSound(GOLDSTERNHALTSND);
			PreloadSound(GOLDSTERNLAUGHSND);
			PreloadSound(WARPINSND);
			PreloadSound(WARPOUTSND);
			PreloadSound(ELEV_BUTTONSND);
#ifdef GAMEVER_RESTORATION_AOG
			PreloadBonus(bo_gold_key);
#endif
#ifndef GAMEVER_RESTORATION_AOG
		// fallthrough
		case gold_morphingobj:
			PreloadSpriteRange(SPR_GOLD_MORPH1, SPR_GOLD_MORPH8);
		// fallthrough
		case gold_morphobj:
			PreloadSpriteRange(SPR_MGOLD_OUCH, SPR_MGOLD_ATTACK4);
			PreloadSound(ELECTSHOTSND);
			PreloadSound(PODDEATHSND);
		// fallthrough
		case goldmorphshotobj:
			PreloadSpriteRange(SPR_MGOLD_SHOT1, SPR_MGOLD_SHOT_EXP3);
#endif
			break;
		case floatingbombobj:
			if (!(obj->flags & FL_STATIONARY))
				PreloadSound(SCOUT_ALERTSND);
		// fallthrough
		case volatiletransportobj:
			PreloadSound(EXPLODE1SND);
			PreloadSound(EXPLODE2SND);
			PreloadSpriteRange(SPR_GSCOUT_W1_1, SPR_GSCOUT_DEAD);
			break;
#ifdef GAMEVER_RESTORATION_AOG
		case vitalobj:
			PreloadSpriteRange(SPR_VITAL_STAND, SPR_VITAL_OUCH);
			PreloadSound(VITAL_GONESND);
			break;
#else
		case rotating_cubeobj:
			PreloadSpriteRange(SPR_CUBE1, SPR_DEAD_CUBE);
			PreloadSound(EXPLODE1SND);
			PreloadSound(ROLL_SCORESND);
			PreloadSound(VITAL_GONESND);
			break;
#endif
#ifndef GAMEVER_RESTORATION_AOG
		case morphing_spider_mutantobj:
			PreloadSpriteRange(SPR_BOSS1_MORPH1, SPR_BOSS1_MORPH9);
			PreloadSound(SCANHALTSND);
		// fallthrough
#endif
		case spider_mutantobj:
			PreloadSpriteRange(SPR_BOSS1_W1, SPR_BOSS1_OUCH);
			PreloadSound(SPITATTACKSND);
			PreloadSound(BLUEBOYDEATHSND);
#ifdef GAMEVER_RESTORATION_AOG
			PreloadBonus(bo_gold_key);
#endif
		// fallthrough
		case spider_mutantshotobj:
			PreloadSpriteRange(SPR_BOSS1_PROJ1, SPR_BOSS1_EXP3);
			break;
		case breather_beastobj:
			PreloadSpriteRange(SPR_BOSS2_W1, SPR_BOSS2_OUCH);
			PreloadSound(GGUARDHALTSND);
			PreloadSound(GGUARDDEATHSND);
#ifdef GAMEVER_RESTORATION_AOG
			PreloadBonus(bo_gold_key);
#endif
		// fallthrough
		case breather_beastshotobj:
			// UNUSED
			break;
		case cyborg_warriorobj:
			PreloadSpriteRange(SPR_BOSS3_W1, SPR_BOSS3_OUCH);
			PreloadSound(BLUEBOYHALTSND);
			PreloadSound(ROBOT_SERVOSND);
			PreloadSound(GGUARDDEATHSND);
#ifdef GAMEVER_RESTORATION_AOG
			PreloadBonus(bo_gold_key);
#else
			PreloadBonus(bo_clip2);
#endif
		// fallthrough
		case cyborg_warriorshotobj:
			// UNUSED
			break;
#ifndef GAMEVER_RESTORATION_AOG
		case morphing_reptilian_warriorobj:
			PreloadSpriteRange(SPR_BOSS4_MORPH1, SPR_BOSS4_MORPH9);
			PreloadSound(GGUARDHALTSND);
		// fallthrough
#endif
		case reptilian_warriorobj:
			PreloadSpriteRange(SPR_BOSS4_W1, SPR_BOSS4_OUCH);
			PreloadSound(GGUARDHALTSND);
			PreloadSound(SCANDEATHSND);
#ifdef GAMEVER_RESTORATION_AOG
			PreloadBonus(bo_gold_key);
#else
			PreloadBonus(bo_clip2);
#endif
		// fallthrough
		case reptilian_warriorshotobj:
			// UNUSED
			break;
		case acid_dragonobj:
			PreloadSpriteRange(SPR_BOSS5_W1, SPR_BOSS5_OUCH);
			PreloadSound(PODHALTSND);
			PreloadSound(SPITATTACKSND);
#ifdef GAMEVER_RESTORATION_AOG
			PreloadBonus(bo_gold_key);
#endif
		// fallthrough
		case acid_dragonshotobj:
			PreloadSpriteRange(SPR_BOSS5_PROJ1, SPR_BOSS5_EXP3);
			break;
		case mech_guardianobj:
			PreloadSpriteRange(SPR_BOSS6_W1, SPR_BOSS6_OUCH);
			PreloadSound(BLUEBOYHALTSND);
			PreloadSound(ROBOT_SERVOSND);
			PreloadSound(DOGBOYDEATHSND);
#ifdef GAMEVER_RESTORATION_AOG
			PreloadBonus(bo_gold_key);
#else
			PreloadBonus(bo_clip2);
#endif
		// fallthrough
		case mech_guardianshotobj:
			// UNUSED
			break;
#ifndef GAMEVER_RESTORATION_AOG
		case final_boss1obj:
			PreloadSpriteRange(SPR_BOSS7_W1, SPR_BOSS7_OUCH);
			PreloadSound(BLUEBOYHALTSND);
			PreloadSound(BLUEBOYDEATHSND);
#ifdef GAMEVER_RESTORATION_AOG
			if (!(gamestate.weapons & (1<<wp_pistol)))
			{
				PreloadBonus(bo_pistol);
			}
			PreloadBonus(bo_clip2);
#endif
			break;
		case final_boss2obj:
			PreloadSpriteRange(SPR_BOSS8_W1, SPR_BOSS8_OUCH);
			PreloadSound(GGUARDHALTSND);
			PreloadSound(ELECTSHOTSND);
			PreloadSound(ELECTSHOTSND);
		// fallthrough
		case final_boss2shotobj:
			// uses goldmorphshotobj
			PreloadSpriteRange(SPR_MGOLD_SHOT1, SPR_MGOLD_SHOT_EXP3);
			break;
		case final_boss3obj:
			PreloadSpriteRange(SPR_BOSS9_W1, SPR_BOSS9_OUCH);
			PreloadSound(BLUEBOYHALTSND);
			PreloadSound(DOGBOYDEATHSND);
			break;
		case final_boss4obj:
			PreloadSpriteRange(SPR_BOSS10_W1, SPR_BOSS10_OUCH);
			PreloadSound(GGUARDHALTSND);
			PreloadSound(SPITATTACKSND);
			PreloadSound(LCANDEATHSND);
		// fallthrough
		case final_boss4shotobj:
			PreloadSpriteRange(SPR_BOSS10_SPIT1, SPR_BOSS10_SPIT_EXP3);
			break;
#endif
		case blakeobj:
			PreloadSpriteRange(SPR_BLAKE_W1, SPR_BLAKE_W4);
			break;
		case crate1obj:
		case crate2obj:
		case crate3obj:
			//printf("%s statinfo type %d\n", __FUNCTION__, statinfo[obj->temp2].type);
			PreloadSprite(statinfo[obj->temp2].picnum);
			PreloadBonus(statinfo[obj->temp2].type);
			// explosionobj or deadobj
			PreloadSpriteRange(SPR_GRENADE_EXPLODE2,SPR_GRENADE_EXPLODE5);
			break;
		case green_oozeobj:
			PreloadSpriteRange(SPR_GREEN_OOZE1, SPR_GREEN_OOZE3);
			break;
		case black_oozeobj:
			PreloadSpriteRange(SPR_BLACK_OOZE1, SPR_BLACK_OOZE3);
			break;
#ifndef GAMEVER_RESTORATION_AOG
		case green2_oozeobj:
			PreloadSpriteRange(SPR_GREEN2_OOZE1, SPR_GREEN2_OOZE3);
			break;
		case black2_oozeobj:
			PreloadSpriteRange(SPR_BLACK2_OOZE1, SPR_BLACK2_OOZE3);
			break;
#endif
		case post_barrierobj:
			PreloadSpriteRange(SPR_ELEC_POST1, SPR_ELEC_POST4);
			break;
		case arc_barrierobj:
			PreloadSpriteRange(SPR_ELEC_ARC1, SPR_ELEC_ARC4);
			PreloadSound(ELECARCDAMAGESND);
			break;

#ifndef GAMEVER_RESTORATION_AOG
		case vpost_barrierobj:
			PreloadSpriteRange(SPR_VPOST1, SPR_VPOST8);
			break;
		case vspike_barrierobj:
			PreloadSpriteRange(SPR_VSPIKE1, SPR_VSPIKE8);
			break;
#endif
		case security_lightobj:
			PreloadSpriteRange(SPR_SECURITY_NORMAL, SPR_SECURITY_ALERT);
			break;

		/* TODO
		explosionobj,
			SPR_GRENADE_EXPLODE1
			SPR_EXPLOSION_1
			PreloadSound(EXPLODE1SND);
			PreloadSound(EXPLODE2SND);
			*/
		case steamgrateobj: // TODO unused?
			PreloadSpriteRange(SPR_GRATE, SPR_STEAM_4);
			break;
		case steampipeobj: // TODO unused?
			PreloadSpriteRange(SPR_STEAM_PIPE, SPR_PIPE_STEAM_4);
			break;
		case ventdripobj: // TODO unused?
			PreloadSpriteRange(SPR_BLOOD_DRIP1, SPR_BLOOD_DRIP4);
			PreloadSpriteRange(SPR_WATER_DRIP1, SPR_WATER_DRIP4);
		break;
		//playerspshotobj, 134 UNUSED
		case flickerlightobj:
			PreloadSpriteRange(SPR_DECO_ARC_1, SPR_DECO_ARC_3);
			break;

#ifndef GAMEVER_RESTORATION_AOG
		case plasma_detonator_reserveobj: // TODO
		case plasma_detonatorobj:
			PreloadSound(ROBOT_SERVOSND);
			PreloadSpriteRange(SPR_DETONATOR_EXP1, SPR_DETONATOR_EXP8);
			// doorexplodeobj
			PreloadSpriteRange(SPR_EXPLOSION_1, SPR_EXPLOSION_5);
			break;
#endif
		case grenadeobj:
			PreloadSpriteRange(SPR_GRENADE_FLY1, SPR_GRENADE_FLY4);
			// explosionobj
			PreloadSpriteRange(SPR_GRENADE_EXPLODE1, SPR_GRENADE_EXPLODE5);
		// fallthrough
#ifndef GAMEVER_RESTORATION_AOG
		case gr_explosionobj:
			PreloadSpriteRange(SPR_EXPLOSION_1, SPR_EXPLOSION_5);
			PreloadSound(EXPLODE1SND);
			PreloadSound(EXPLODE2SND);
#else
			// TODO explosionobj
#endif
			break;
#ifndef GAMEVER_RESTORATION_AOG
		case bfg_shotobj:
			PreloadSpriteRange(SPR_BFG_WEAPON_SHOT1, SPR_BFG_WEAPON_SHOT3);
		// fallthrough
		case bfg_explosionobj:
			PreloadSpriteRange(SPR_BFG_EXP1, SPR_BFG_EXP8);
			PreloadSound(EXPLODE1SND);
			PreloadSound(EXPLODE2SND);
		// fallthrough
		case doorexplodeobj:
			PreloadSpriteRange(SPR_EXPLOSION_1, SPR_EXPLOSION_5);
			PreloadSound(EXPLODE1SND);
			PreloadSound(EXPLODE2SND);
			break;
		case pd_explosionobj:
			PreloadSpriteRange(SPR_DETONATOR_EXP1, SPR_DETONATOR_EXP8);
			PreloadSound(EXPLODE1SND);
			PreloadSound(EXPLODE2SND);
			break;
#endif
		default:
			printf("%s unknown object class %d\n", __FUNCTION__, obj->obclass);
	}
}

static objtype preloadobj;

static void PreloadWeapon(weapontype weapon)
{
	switch (weapon)
	{
		case wp_autocharge:
			PreloadSpriteRange(SPR_KNIFEREADY, SPR_KNIFEATK4);
			PreloadSound(ATKAUTOCHARGESND);
			break;
		case wp_pistol:
			PreloadSpriteRange(SPR_PISTOLREADY, SPR_PISTOLATK4);
			PreloadSound(ATKCHARGEDSND);
			break;
		case wp_burst_rifle:
			PreloadSpriteRange(SPR_MACHINEGUNREADY, SPR_MACHINEGUNATK4);
			PreloadSound(ATKBURSTRIFLESND);
			break;
		case wp_ion_cannon:
			PreloadSpriteRange(SPR_CHAINREADY, SPR_CHAINATK4);
			PreloadSound(ATKIONCANNONSND);
			break;
		case wp_grenade:
			PreloadSpriteRange(SPR_GRENADEREADY, SPR_GRENADEATK4);
			PreloadSpriteRange(SPR_GRENADE_FLY1, SPR_GRENADE_EXPLODE5);
			PreloadSound(ATKGRENADESND);
			preloadobj.obclass = grenadeobj;
			PreloadObject(&preloadobj);
			break;
#ifndef GAMEVER_RESTORATION_AOG
		case wp_bfg_cannon:
			PreloadSpriteRange(SPR_BFG_WEAPON1, SPR_BFG_WEAPON5);
			PreloadSpriteRange(SPR_BFG_WEAPON_SHOT1, SPR_BFG_WEAPON_SHOT3);
			PreloadSpriteRange(SPR_BFG_EXP1, SPR_BFG_EXP8);
			PreloadSound(ATKIONCANNONSND);
			preloadobj.obclass = bfg_shotobj;
			PreloadObject(&preloadobj);
			break;
#endif
	}
}

void PreloadGotPages(boolean loadingbar)
{
	// round 1: free the pages that are no longer needed, count the new ones
	int newpages = 0;
	int freedpages = 0;
	for (int i = 0; i < ChunksInFile; i++)
	{
		boolean haspage = PM_GetPageAddress(i) != NULL;
		boolean needpage = TEST_GOTPAGE(i);
		//printf("%s page %d had %d need %d\n", __FUNCTION__, i, haspage, needpage);
		if (haspage && !needpage && i != ChunksInFile-1)
		{
			//printf("%s page %d is no longer needed\n", __FUNCTION__, i);
			PM_FreePage(i);
			freedpages++;
		}
		else if (!haspage && needpage)
		{
			//printf("%s page %d will be loaded\n", __FUNCTION__, i);
			//PM_GetPage(i);
			newpages++;
		}
	}
	//printf("%s new pages %d freed pages %d\n", __FUNCTION__, newpages, freedpages);
	// round 2: load the new pages
	int loadedpages = 0;
	uint32_t lasttime = 0;
	for (int i = 0; i < ChunksInFile; i++)
	{
		boolean haspage = PM_GetPageAddress(i) != NULL;
		boolean needpage = TEST_GOTPAGE(i);
		if (!haspage && needpage)
		{
			//printf("%s loading page %d...\n", __FUNCTION__, i);
			if (loadingbar && TimeCount - lasttime > TickBase / 10)
			{
				PreloadUpdate(loadedpages, newpages);
				lasttime = TimeCount;
			}
			PM_GetPage(i);
			loadedpages++;
		}
	}
}
#endif

void PreloadGraphics(void)
{
	WindowY=188;

	if (!(gamestate.flags & GS_QUICKRUN))
		VW_FadeIn ();

#ifdef __AMIGA__
	memset(gotpage, 0, sizeof(gotpage));

	// walls and doors
	for (int16_t y=0;y<mapheight;y++)
		for (int16_t x=0;x<mapwidth;x++)
		{
			uint16_t tile = tilemap[x][y];
			if (tile > 0 && tile < MAXWALLTILES)
			{
				//printf("%s x %2d y %2d tile %2d horizwall %d vertwall %d\n", __FUNCTION__, x, y, tile, horizwall[tile], vertwall[tile]);
				PreloadWall(horizwall[tile]);
				PreloadWall(vertwall[tile]);
				if (tile == ON_SWITCH)
				{
					PreloadWall(vertwall[OFF_SWITCH]);
					PreloadWall(horizwall[OFF_SWITCH]);
					PreloadSound(SWITCHSND);
				}
				else if (tile == OFF_SWITCH)
				{
					PreloadWall(vertwall[ON_SWITCH]);
					PreloadWall(horizwall[ON_SWITCH]);
					PreloadSound(SWITCHSND);
				}
			}
			else if (tile & 0x80)
			{
#define DOORWALL	(PMSpriteStart-(NUMDOORTYPES))
				uint8_t doornum = tile & 0x7f;
				door_t type = doorobjlist[doornum].type;
				const uint16_t doorpages[] = {L_BIO, L_METAL, L_PRISON, L_ELEVATOR, L_HIGH_SECURITY, L_HIGH_TECH, L_ENTER_ONLY, L_ENTER_ONLY, L_ENTER_ONLY, L_ENTER_ONLY, L_SPACE};
				extern uint16_t far DoorJams[];
				uint16_t doorpage = DOORWALL + doorpages[type];
				uint16_t doorjam = DOORWALL + DoorJams[type];
				boolean twoway = (type < dr_oneway_left) || (type > dr_oneway_down);
				if (doorobjlist[doornum].vertical)
				{
					if (type != dr_bio) // bio doors are not shaded in HitVertDoor
						doorpage++;
				}
				else
				{
					doorjam++;
				}

				//printf("%s x %2d y %2d door type %2d doorpage %d doorjam %d\n", __FUNCTION__, x, y, type, doorpage, doorjam);
				PreloadWall(doorpage);
				PreloadWall(doorjam);

				// TODO check if the door is initially locked
				//if (lockable)
				{
					doorpage += UL_METAL;
					//printf("%s x %2d y %2d lockable %2d doorpage %d\n", __FUNCTION__, x, y, type, doorpage);
					PreloadWall(doorpage);
				}
				if (!twoway)
				{
					doorpage = DOORWALL + NOEXIT;
					if (doorobjlist[doornum].vertical)
						doorpage++;
					//printf("%s x %2d y %2d oneway %2d doorpage %d\n", __FUNCTION__, x, y, type, doorpage);
					PreloadWall(doorpage);
				}
				
				switch(type)
				{
					case dr_bio:
					case dr_office:
					case dr_space:
					case dr_normal:
						PreloadSound(HTECHDOOROPENSND);
						PreloadSound(HTECHDOORCLOSESND);
					break;

					default:
						PreloadSound(OPENDOORSND);
						PreloadSound(CLOSEDOORSND);
					break;
				}
			}
			else if (tile & 0x40)
			{
				tile &= ~0x40; // clear the adjacent door marker bit
				//printf("%s x %2d y %2d tile %2d horizwall %d vertwall %d\n", __FUNCTION__, x, y, tile, horizwall[tile], vertwall[tile]);
				PreloadWall(horizwall[tile]);
				PreloadWall(vertwall[tile]);
			}
		}

	// static objects
	for (statobj_t *statptr = &statobjlist[0] ; statptr != laststatobj ; statptr++)
	{
		if (statptr->shapenum == -1)
			continue;

		//printf("%s statobj type %d\n", __FUNCTION__, statptr->shapenum);
		PreloadSprite(statptr->shapenum);
		if (statptr->flags & FL_BONUS)
		{
			//printf("%s statobj itemnumber %d\n", __FUNCTION__, statptr->itemnumber);
			PreloadBonus(statptr->itemnumber);
		}
	}

	// active objects
	for (objtype *obj = player->next;obj;obj=obj->next)
	{
		//printf("%s obclass %d flags %08x\n", __FUNCTION__, obj->obclass, obj->flags);
		PreloadObject(obj);
#ifndef GAMEVER_RESTORATION_AOG
		if (obj->flags2 & FL2_DROP_RKEY)
			PreloadBonus(bo_red_key);

		if (obj->flags2 & FL2_DROP_YKEY)
			PreloadBonus(bo_yellow_key);

		if (obj->flags2 & FL2_DROP_BKEY)
			PreloadBonus(bo_blue_key);

		if (obj->flags2 & FL2_DROP_BFG)
			PreloadBonus(bo_bfg_cannon);

		if (obj->flags2 & FL2_DROP_ION)
			PreloadBonus(bo_ion_cannon);

		if (obj->flags2 & FL2_DROP_DETONATOR)
			PreloadBonus(bo_plasma_detonator);
#endif
	}

	// player weapons
	for (int i=wp_autocharge;i<wp_SPACER;i++)
	{
		if (gamestate.weapons & (1<<i))
		{
			//printf("%s player weapon %d\n", __FUNCTION__, i);
			PreloadWeapon(i);
		}
	}
	// charge unit
#ifdef GAMEVER_RESTORATION_AOG
	PreloadSprite(SPR_STAT_26);
#else
	PreloadSprite(SPR_STAT_31);
#endif
	// misc sound effects
	PreloadSound(NOWAYSND);
	PreloadSound(PUSHWALLSND);
	PreloadSound(ATKCHARGEDSND);
	PreloadSound(GETAMMOSND); // TODO
	PreloadSound(GOTTOKENSND); // TODO
	PreloadSound(HITWALLSND);
	PreloadSound(EXTRA_MANSND);

	if (NumEAWalls)
	{
		preloadobj.obclass = electroobj;
		PreloadObject(&preloadobj);
	}
	if (GoldsternInfo.SpawnCnt)
	{
		preloadobj.obclass = goldsternobj;
		PreloadObject(&preloadobj);
	}

	PreloadGotPages(true);
#else
	PM_Preload(PreloadUpdate);
#endif
	IN_UserInput(70);

	if (playstate != ex_transported)
		VW_FadeOut ();

	DrawPlayBorder ();
	VW_UpdateScreen ();
}

//==========================================================================

/*
==================
=
= DrawHighScores
=
==================
*/

#define SCORE_Y_SPACING			7			//

void	DrawHighScores(void)
{
	char		buffer[16],*str;
	word		i,
				w,h;
	HighScore	*s;

	ClearMScreen();
	CA_CacheScreen (BACKGROUND_SCREENPIC);
	DrawMenuTitle("HIGH SCORES");

	if (playstate != ex_title)
		DrawInstructions(IT_HIGHSCORES);

	fontnumber=2;
	SETFONTCOLOR(ENABLED_TEXT_COLOR,TERM_BACK_COLOR);

	ShadowPrint("NAME",86,60);
	/*** BLAKE STONE: ALIENS OF GOLD RESTORATION ***/
	// Re-enable code for AOG
#ifdef GAMEVER_RESTORATION_AOG
	ShadowPrint("MISSION",150,60);
#endif
	/*** BLAKE STONE: ALIENS OF GOLD RESTORATION ***/
#ifdef GAMEVER_RESTORATION_AOG
	ShadowPrint("SCORE",205,60);
#else
	ShadowPrint("SCORE",175,60);
#endif
	ShadowPrint("MISSION",247,53);
	ShadowPrint("RATIO",254,60);

	for (i = 0,s = Scores;i < MaxScores;i++,s++)
	{
		SETFONTCOLOR(HIGHLIGHT_TEXT_COLOR-1,TERM_BACK_COLOR);
		//
		// name
		//
		if (*s->name)
			ShadowPrint(s->name,45,68 + (SCORE_Y_SPACING * i));

		/*** BLAKE STONE: ALIENS OF GOLD RESTORATION ***/
		// Re-enable code for AOG
#ifdef GAMEVER_RESTORATION_AOG
//#if 0
		//
		// mission
		//

		ltoa(s->episode+1,buffer,10);
		ShadowPrint(buffer,165,68 + (SCORE_Y_SPACING * i));
#endif

		//
		// score
		//

		if (s->score > 9999999)
			SETFONTCOLOR(HIGHLIGHT_TEXT_COLOR+1,TERM_BACK_COLOR);

		ltoa(s->score,buffer,10);
		USL_MeasureString(buffer,&w,&h);
		/*** BLAKE STONE: ALIENS OF GOLD RESTORATION ***/
		// Should explain the comment mentioning 235
#ifdef GAMEVER_RESTORATION_AOG
		ShadowPrint(buffer,235 - w,68 + (SCORE_Y_SPACING * i));
#else
		ShadowPrint(buffer,205 - w,68 + (SCORE_Y_SPACING * i));		// 235
#endif

		//
		// mission ratio
		//
		ltoa(s->ratio,buffer,10);
		USL_MeasureString(buffer,&w,&h);
		ShadowPrint(buffer,272-w,68 + (SCORE_Y_SPACING * i));
	}

	VW_UpdateScreen ();
}

//===========================================================================


/*
=======================
=
= CheckHighScore
=
=======================
*/

void	CheckHighScore (int32_t score,word other)
{
	word		i,j;
	int16_t			n;
	HighScore	myscore;
	US_CursorStruct TermCursor = {'@',0,HIGHLIGHT_TEXT_COLOR,2};


	/*** BLAKE STONE: ALIENS OF GOLD V1.0 RESTORATION ***/
#ifndef GAMEVER_RESTORATION_AOG_100
   // Check for cheaters

   if (DebugOk)
	{
   	SD_PlaySound(NOWAYSND);
      return;
   }
#endif

	strcpy(myscore.name,"");
	myscore.score = score;
	myscore.episode = gamestate.episode;
	myscore.completed = other;
	/*** BLAKE STONE: ALIENS OF GOLD RESTORATION ***/
#ifdef GAMEVER_RESTORATION_AOG
	myscore.ratio = ShowStats(0,0,ss_justcalc);
#else
	myscore.ratio = ShowStats(0,0,ss_justcalc,&gamestuff.level[gamestate.mapon].stats);
#endif

	for (i = 0,n = -1;i < MaxScores;i++)
	{
		if ((myscore.score > Scores[i].score) ||	((myscore.score == Scores[i].score)
			&& (myscore.completed > Scores[i].completed)))
		{
			for (j = MaxScores;--j > i;)
				Scores[j] = Scores[j - 1];
			Scores[i] = myscore;
			n = i;
			break;
		}
	}

	StartCPMusic (ROSTER_MUS);
	DrawHighScores ();

	VW_FadeIn ();

	if (n != -1)
	{
		//
		// got a high score
		//

		DrawInstructions(IT_ENTER_HIGHSCORE);
		SETFONTCOLOR(HIGHLIGHT_TEXT_COLOR,TERM_BACK_COLOR);
		PrintY = 68+(SCORE_Y_SPACING * n);
		PrintX = 45;
		use_custom_cursor = true;
		allcaps = true;
		US_CustomCursor = TermCursor;
		US_LineInput(PrintX,PrintY,Scores[n].name,nil,true,MaxHighName,100);
	}
	else
	{
		IN_ClearKeysDown ();
		IN_UserInput(500);
	}

   StopMusic();
	use_custom_cursor = false;
}

//===========================================================================

//--------------------------------------------------------------------------
// Random()
//--------------------------------------------------------------------------
uint16_t Random(uint16_t Max)
{
	uint16_t returnval;

   if (Max)
   {
		if (Max > 255)
   		returnval = (US_RndT()<<8) + US_RndT();
	   else
   		returnval = US_RndT();

		return(returnval % Max);
   }
   else
   	return(0);
}

