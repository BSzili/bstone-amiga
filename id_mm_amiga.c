/* Catacomb 3-D Source Code
 * Copyright (C) 1993-2014 Flat Rock Software
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

// Lightweight memory manager using the C library allocation functions

#include "id_heads.h"
#define GRAPHICS_DISPLAYINFO_H
#include <proto/exec.h>

#define USE_NODE_SIZE
#define USE_MEMPOOL
//#define LOCKBIT			0x80	// if set in attributes, block cannot be moved
#define PURGEBITS			3		// 0-3 level, 0= unpurgable, 3= purge first
#define PURGEMASK			0xfffc
//#define BASEATTRIBUTES	0		// unlocked, non purgable

typedef struct mmnodestruct
{
	struct MinNode node;
	memptr		*useptr;	// pointer to the segment start
	uint16_t attributes;
#ifdef USE_NODE_SIZE
	uint32_t size;
#endif
} mmnodetype;

mminfotype	mminfo;
memptr		bufferseg;
boolean		mmerror;

void		(* beforesort) (void);
void		(* aftersort) (void);

boolean		mmstarted;
boolean		bombonerror;
static struct MinList memlist;
#ifdef USE_MEMPOOL
static APTR mempool;
#endif

void MM_Startup (void)
{
	if (mmstarted)
		MM_Shutdown ();

	mmstarted = true;
	bombonerror = true;

#ifdef USE_MEMPOOL
	mempool = CreatePool(MEMF_FAST, PMPageSize, PMPageSize);
	if (!mempool)
		return; // it's over, man.
#endif

	mminfo.nearheap = AvailMem(MEMF_FAST);
	mminfo.farheap = AvailMem(MEMF_CHIP);
	mminfo.mainmem = mminfo.nearheap + mminfo.farheap;
	mminfo.EMSmem = 0;
	mminfo.XMSmem = 0;

	NewList(&memlist);

	MM_GetPtr (&bufferseg,BUFFERSIZE);
}

void MM_Shutdown (void)
{
	if (!mmstarted)
		return;

	MM_FreePtr (&bufferseg);
#ifdef USE_MEMPOOL
	if (mempool)
	{
		DeletePool(mempool);
		mempool = NULL;
	}
#endif
}

static mmnodetype *MML_GetNewNode(uint32_t size)
{
	mmnodetype *node;

#ifdef USE_MEMPOOLS
	node = AllocPooled(poolHeader, sizeof(mmnodetype) + size);
#else
	node = calloc(1, sizeof(mmnodetype) + size);
#endif
	if (node)
	{
#ifdef USE_NODE_SIZE
		node->size = size;
#endif
		AddTail((struct List *)&memlist, (struct Node *)node);
	}

	return node;
}

static void MML_PurgeNodes(void)
{
	mmnodetype *node, *node2;

	VW_ColorBorder(15);
	// not very sophisticated, but the game only uses purge level 0 and 3
	for (node = (mmnodetype *)memlist.mlh_Head; (node2 = (mmnodetype *)node->node.mln_Succ); node = node2)
	{
		if (!(node->attributes&LOCKBIT) && (node->attributes&PURGEBITS))
		{
			//bug("%s purging node %p\node", __FUNCTION__, node);
			MM_FreePtr(node->useptr);
		}
	}
	VW_ColorBorder(0);
}

void MM_GetPtr (memptr *baseptr,uint32_t size)
{
	mmnodetype *node;

	//printf("%s(%p,%u)\n", __FUNCTION__, baseptr, size);

	*baseptr = NULL;
	node = MML_GetNewNode(size);
	if (!node)
	{
		// try to free some purgable nodes
		MML_PurgeNodes();
		node = MML_GetNewNode(size);
	}

	if (node)
	{
		// success
		*baseptr = (memptr)(node + 1);
		node->useptr = baseptr;
		//bug("%s %p -> %p %u\n", __FUNCTION__, baseptr, *baseptr, size);
	}

	if (*baseptr)
		return;

	if (bombonerror)
		Quit ("MM_GetPtr: Out of memory!");
	else
		mmerror = true;
}

void MM_FreePtr (memptr *baseptr)
{
	mmnodetype *node;

	//printf("%s(%p)\n", __FUNCTION__, baseptr);

	if (!baseptr || !*baseptr)
		return;

	node = ((mmnodetype *)*baseptr - 1);
	Remove((struct Node *)node);
#ifdef USE_MEMPOOLS
	FreePooled(mempool, node, sizeof(mmnodetype) + node->size);
#else
	free(node);
#endif

	//bug("%s %p -> %p\n", __FUNCTION__, baseptr, *baseptr);
	*baseptr = NULL;
}

void MM_SetPurge (memptr *baseptr, int16_t purge)
{
	mmnodetype *node;

	//printf("%s(%p,%d)\n", __FUNCTION__, baseptr, purge);

	if (!baseptr || !*baseptr)
		return;

	node = ((mmnodetype *)*baseptr - 1);
	node->attributes &= ~PURGEBITS;
	node->attributes |= purge;
}

void MM_SetLock (memptr *baseptr, boolean locked)
{
	mmnodetype *node;

	//printf("%s(%p,%d)\n", __FUNCTION__, baseptr, locked);

	if (!baseptr || !*baseptr)
		return;

	node = ((mmnodetype *)*baseptr - 1);
	node->attributes &= ~LOCKBIT;
	node->attributes |= locked*LOCKBIT;
}

void MM_SortMem (void)
{
	//MML_PurgeNodes();
}

void MM_ShowMemory (void)
{
}

int32_t MM_UnusedMemory (void)
{
	return AvailMem(MEMF_ANY);
}

int32_t MM_TotalFree (void)
{
	uint32_t free;
	mmnodetype *node;

	free = AvailMem(MEMF_ANY);
#ifdef USE_NODE_SIZE
	for (node = (mmnodetype *)memlist.mlh_Head; node->node.mln_Succ; node = (mmnodetype *)node->node.mln_Succ)
	{
		if (!(node->attributes&LOCKBIT) && (node->attributes&PURGEBITS))
		{
			//bug("free %lu size %lu\n", free, node->size);
			free += node->size;
		}
	}
#endif
	return free;
}

int32_t MM_LargestAvail (void)
{
	// this will force the movie buffer size to 64256
	return 65535;
}

void MM_BombOnError (boolean bomb)
{
	bombonerror = bomb;
}
