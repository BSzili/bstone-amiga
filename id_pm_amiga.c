#include "3d_def.h"
#undef PM_GetPage
#define GRAPHICS_DISPLAYINFO_H
#include <proto/exec.h>

//	EMS specific variables
	word				EMSPagesAvail;

//	XMS specific variables
	word				XMSPagesAvail;

//	File specific variables
	char				PageFileName[13] = {"VSWAP."};

#ifndef GAMEVER_RESTORATION_AOG_100
   shadfile_t 		FileUsed = sd_NONE;
#endif
#if DUAL_SWAP_FILES
	char				AltPageFileName[13] = {"SVSWAP."};
	/*** BLAKE STONE: ALIENS OF GOLD V1.0 RESTORATION ***/
#ifndef GAMEVER_RESTORATION_AOG_100
	boolean ShadowsAvail = false;
#endif
#endif

static boolean PMStarted;

static int PageFile = -1;
word ChunksInFile, PMSpriteStart, PMSoundStart;

PageListStruct *PMPages;
uint8_t **PMData;

static void PML_ReadFromFile(byte *buf, long offset, word length)
{
	if (!buf)
		Quit("PML_ReadFromFile: Null pointer");
	if (!offset)
		Quit("PML_ReadFromFile: Zero offset");
	if (lseek(PageFile,offset,SEEK_SET) != offset)
		Quit("PML_ReadFromFile: Seek failed");
	if (read(PageFile,buf,length) != length)
		Quit("PML_ReadFromFile: Read failed");
}

void PML_OpenPageFile(char *filename)
{
	int i;
	PageListStruct *page;

	PageFile = open(filename,O_RDONLY + O_BINARY);
	if (PageFile == -1)
		Quit("PML_OpenPageFile: Unable to open page file");

	/* Read in header variables */
	ChunksInFile = 0;
	read(PageFile,&ChunksInFile,sizeof(ChunksInFile));
	PMSpriteStart = 0;
	read(PageFile,&PMSpriteStart,sizeof(PMSpriteStart));
	PMSoundStart = 0;
	read(PageFile,&PMSoundStart,sizeof(PMSoundStart));
#ifdef __AMIGA__
	ChunksInFile = SWAP16LE(ChunksInFile);
	PMSpriteStart = SWAP16LE(PMSpriteStart);
	PMSoundStart = SWAP16LE(PMSoundStart);
	//printf("%s %d %d %d\n", __FUNCTION__, ChunksInFile, PMSpriteStart, PMSoundStart);
#endif

	MM_GetPtr((memptr)&PMData, (ChunksInFile + 1) * sizeof(uint8_t *));
	MM_SetLock((memptr)&PMData, true);

	/* Allocate and clear the page list */
	MM_GetPtr((memptr)&PMPages, sizeof(PageListStruct) * ChunksInFile);
	MM_SetLock((memptr)&PMPages, true);

	/* Read in the chunk offsets */
	for (i = 0, page = PMPages; i < ChunksInFile; i++, page++) {
		//page->offset = ReadInt32(PageFile);
		read(PageFile, &page->offset, sizeof(uint32_t));
#ifdef __AMIGA__
		page->offset = SWAP32LE(page->offset);
		//printf("%s page %d offset %d\n", __FUNCTION__, i, page->offset);
#endif
	}

	/* Read in the chunk lengths */
	for (i = 0, page = PMPages; i < ChunksInFile; i++, page++) {
		//page->length = ReadInt16(PageFile);
		page->length = 0;
		read(PageFile, &page->length, sizeof(word));
#ifdef __AMIGA__
		page->length = SWAP16LE(page->length);
		//printf("%s page %d length %d\n", __FUNCTION__, i, page->length);
#endif
	}
	
	// adjust the sizes of the sounds that span multiple pages
	word *soundInfoPage = (word *)PM_GetPage(ChunksInFile - 1);
	word NumDigi = (word) PM_GetPageSize(ChunksInFile - 1) / 4;
	for (i = 0; i < NumDigi; i++) {
		word startpage = soundInfoPage[i * 2] + PMSoundStart;
		word size = soundInfoPage[i * 2 + 1];
		//printf("%d digi %d size %u -> %u\n", i, startpage, PMPages[startpage].length, size);
		PMPages[startpage].length = size;
	}
	//Quit("That's all");
}

static void PML_ClosePageFile()
{
	if (PageFile != -1) {
		close(PageFile);
		PageFile = -1;
	}

	for (int i = 0; i < ChunksInFile; i++) {
		PM_FreePage(i);
	}

	if (PMData) {
		MM_SetLock((memptr)&PMData,false);
		MM_FreePtr((memptr)&PMData);
	}

	if (PMPages) {
		MM_SetLock((memptr)&PMPages,false);
		MM_FreePtr((memptr)&PMPages);
	}
}

word PM_GetPageSize(int16_t pagenum)
{
	if (pagenum >= ChunksInFile)
		Quit("PM_GetPage: Invalid page request");

	return PMPages[pagenum].length;
}

memptr PM_GetPageAddress(int16_t pagenum)
{
	return PMData[pagenum];
}

memptr PM_GetPage(int16_t pagenum)
{
	PageListStruct *page;
	uint8_t *addr;

	if (pagenum >= ChunksInFile)
		Quit("PM_GetPage: Invalid page request");

	addr = PMData[pagenum];

	if (addr == NULL) {
		page = &PMPages[pagenum];
		boolean soundpage = (pagenum >= PMSoundStart && pagenum < ChunksInFile - 1);
		uint32_t offset = page->offset;
		word length = page->length;
		if (soundpage)
		{
			PMData[pagenum] = AllocVec(length, MEMF_CHIP);
		}
		else
		{
			MM_GetPtr((memptr)&PMData[pagenum], length);
			//MM_SetPurge((memptr)&PMData[pagenum], 3);
		}
		if (!PMData[pagenum]) Quit("PM_GetPage: failed to allocate %d bytes\n", length);
		addr = PMData[pagenum];
		//if (!playstate && !startgame && frameon) printf("%s loading %d\n", __FUNCTION__, pagenum);

		PML_ReadFromFile(addr, offset, length);
#ifdef __AMIGA__
		if (pagenum < PMSpriteStart)
		{
			// walls
			//VL_RemapBufferEHB(addr, addr, length);
		}
		else if (pagenum < PMSoundStart)
		{
			// sprites
			//printf("%s(%d) byteswapping sprite\n", __FUNCTION__, pagenum);
			t_compshape *shape = (t_compshape *)addr;
			shape->leftpix = SWAP16LE(shape->leftpix);
			shape->rightpix = SWAP16LE(shape->rightpix);
			uint16_t swidth = shape->rightpix - shape->leftpix;

			for (uint16_t texturecolumn = 0; texturecolumn <= swidth; texturecolumn++)
			{
				shape->dataofs[texturecolumn] = SWAP16LE(shape->dataofs[texturecolumn]);
				uint16_t *srcpost = (uint16_t *)&addr[shape->dataofs[texturecolumn]];
				*srcpost = SWAP16LE(*srcpost); // end
				uint16_t end = *srcpost/2;
				srcpost++;
				while (end != 0)
				{
					*srcpost = SWAP16LE(*srcpost); // source
					uint16_t source = *srcpost;
					srcpost++;
					*srcpost = SWAP16LE(*srcpost); // start
					uint16_t start = *srcpost / 2;
					source += start;
					srcpost++;
					uint16_t length = end - start;
					//VL_RemapBufferEHB(addr+source, addr+source, length);
					*srcpost = SWAP16LE(*srcpost); // end
					end = *srcpost/2;
					srcpost++;
				}
			}
		}
		else if (pagenum >= PMSoundStart && pagenum < ChunksInFile - 1)
		{
			// sound samples
			//printf("%s(%d) fixing samples\n", __FUNCTION__, pagenum);
			for (int j = 0; j < length; j++)
			{
				addr[j] ^= 128;
			}
		}
		else if (pagenum == ChunksInFile - 1)
		{
			// digi info page
			word *soundInfoPage = (word *)addr;
			for (int j = 0; j < length/2; j++)
			{
				soundInfoPage[j] = SWAP16LE(soundInfoPage[j]);
			}
		}
#endif
	}
	return addr;
}

void PM_FreePage(int16_t pagenum)
{
	uint8_t *addr;

	if (pagenum >= ChunksInFile)
		Quit("PM_FreePage: Invalid page request");

	addr = PMData[pagenum];
	if (addr != NULL) {
		boolean soundpage = (pagenum >= PMSoundStart && pagenum < ChunksInFile - 1);
		if (soundpage) {
			FreeVec(addr);
			PMData[pagenum] = NULL;
		} else {
			MM_FreePtr((memptr)&PMData[pagenum]);
		}
	}
}

//#if DUAL_SWAP_FILES
void PM_Startup()
{
	if (PMStarted)
		return;

	OpenPageFile();

	PMStarted = true;
}
//#endif

void PM_Shutdown()
{
	if (!PMStarted)
		return;

	PML_ClosePageFile();

	PMStarted = false;
}

// Blake Stone stubs

void
PM_CheckMainMem(void)
{
	// TODO
}

void
PM_NextFrame(void)
{
	// TODO
}

void
PM_SetMainMemPurge(int16_t level)
{
	// TODO
}

void
PM_SetPageLock(int16_t pagenum,PMLockType lock)
{
	// TODO
}
